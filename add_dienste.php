<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Dienstplan Kalender</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css' rel='stylesheet' />
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js'></script>
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
            font-family: Arial, sans-serif;
        }
        #calendar {
            margin: 20px auto;
            width: 90%;
        }
        .fc-header-toolbar {
            background-color: #ff8c00;
            color: #ffffff;
        }
        .btn-primary {
            background-color: #ff8c00;
            border-color: #ff8c00;
        }
    </style>
</head>
<body>
    <?php include 'navbar.php'; ?>

    <div class="container mt-5">
        <div id="calendar-container">
            <div id='calendar'></div>
        </div>

        <!-- Formular zum Hinzufügen von Diensten -->
        <div id="addDienstForm" class="mt-3">
            <input type="number" class="form-control mb-2" id="arbeitsbereichID" placeholder="Arbeitsbereich ID" required>
            <input type="number" class="form-control mb-2" id="mitarbeiterID" placeholder="Mitarbeiter ID" required>
            <input type="date" class="form-control mb-2" id="dienstDatum" placeholder="Dienstdatum (YYYY-MM-DD)" required>
            <input type="time" class="form-control mb-2" id="startzeit" placeholder="Startzeit (HH:MM:SS)" required>
            <input type="time" class="form-control mb-2" id="endzeit" placeholder="Endzeit (HH:MM:SS)" required>
            <textarea class="form-control mb-2" id="beschreibung" placeholder="Beschreibung" required></textarea>
            <input type="number" class="form-control mb-2" id="anzahlMitarbeiter" placeholder="Anzahl benötigter Mitarbeiter" required>
            <select class="form-control mb-3" id="status" required>
                <option value="FORECAST">FORECAST</option>
                <option value="OFFEN">OFFEN</option>
                <option value="BESETZT">BESETZT</option>
                <option value="FERTIG">FERTIG</option>
            </select>
            <button onclick="addDienst()" class="btn btn-primary">Dienst hinzufügen</button>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: 'dienstplan.php',
                editable: true
            });
        });

        function addDienst() {
            var data = {
                arbeitsbereichID: $('#arbeitsbereichID').val(),
                mitarbeiterID: $('#mitarbeiterID').val(),
                dienstDatum: $('#dienstDatum').val(),
                startzeit: $('#startzeit').val(),
                endzeit: $('#endzeit').val(),
                beschreibung: $('#beschreibung').val(),
                anzahlMitarbeiter: $('#anzahlMitarbeiter').val(),
                status: $('#status').val(),
                benutzer: 'Username' // Dies sollte dynamisch basierend auf dem angemeldeten Benutzer gesetzt werden
            };

            $.post('add_dienst.php', data, function(response) {
                alert('Dienst hinzugefügt: ' + response);
                $('#calendar').fullCalendar('refetchEvents'); // Kalender aktualisieren
            }).fail(function() {
                alert('Fehler beim Hinzufügen des Dienstes.');
            });
        }
    </script>
</body>
</html>
