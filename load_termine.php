<?php
include_once 'database.php';

header('Content-Type: application/json');

$pdo = pdo();
$sql = "SELECT termin_id, arbeitsbereich, datum, beginn, ende, anzahl, status FROM termine";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$termine = $stmt->fetchAll();

$calendarEvents = array();
foreach ($termine as $termin) {
    $event = [
        'title' => $termin['arbeitsbereich'] . ' - ' . $termin['status'],
        'start' => $termin['datum'] . 'T' . $termin['beginn'],
        'end' => $termin['datum'] . 'T' . $termin['ende']
    ];
    array_push($calendarEvents, $event);
}

echo json_encode($calendarEvents);
?>
