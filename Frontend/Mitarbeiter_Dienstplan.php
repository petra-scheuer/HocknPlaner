<?php
include 'navbar.php';

include '../config/database.php'; // Pfad ggf. anpassen

$pdo = pdo(); // PDO-Verbindung herstellen

function getActiveMitarbeiter($pdo) {
    $sql = "SELECT MitarbeiterID, Vorname, Nachname FROM mitarbeiter WHERE Status = 'aktiv' ORDER BY Nachname, Vorname";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getDienstplan($pdo, $mitarbeiterId, $monatJahr) {
    $startDatum = $monatJahr . '-01'; // Erster Tag des gewählten Monats
    $endDatum = date('Y-m-t', strtotime($startDatum)); // Letzter Tag des gewählten Monats
    $sql = "SELECT t.datum, t.beginn, t.ende, TIMESTAMPDIFF(MINUTE, t.beginn, t.ende) as Arbeitsminuten FROM termine t
            INNER JOIN dienst_mitarbeiter dm ON dm.dienst_id = t.termin_id
            WHERE dm.mitarbeiter_id = :mitarbeiterId AND t.datum BETWEEN :startDatum AND :endDatum";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':mitarbeiterId', $mitarbeiterId, PDO::PARAM_INT);
    $stmt->bindParam(':startDatum', $startDatum);
    $stmt->bindParam(':endDatum', $endDatum);
    $stmt->execute();
    return $stmt->fetchAll();
}

// Funktion zur Berechnung der Pause basierend auf der Dauer des Dienstes
function calculatePause($start, $end) {
    $startTime = new DateTime($start);
    $endTime = new DateTime($end);
    $duration = $startTime->diff($endTime)->h + ($startTime->diff($endTime)->i / 60);

    if ($duration <= 6) {
        return 0;
    } elseif ($duration <= 8) {
        return 0.5;
    } else {
        return 1;
    }
}

$dienstpläne = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['mitarbeiter_id'], $_POST['monat'])) {
    $mitarbeiterId = (int)$_POST['mitarbeiter_id'];
    $monatJahr = $_POST['monat']; // Format yyyy-mm
    $dienstpläne = getDienstplan($pdo, $mitarbeiterId, $monatJahr);
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter Dienstplan Suche</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            color: #333;
        }
        .header {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
        .filter-legend {
            margin-top: 10px;
            display: flex;
            justify-content: center;
        }
        .filter-link {
            margin: 0 5px;
        }
 
    </style>
</head>
<body>
<h2 class="header">Mitarbeiter Dienstplan</h2>

<div class="container">
    <form action="" method="post" class="mb-3">
        <div class="form-group">
            <label for="mitarbeiter_id">Mitarbeiter wählen:</label>
            <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                <?php
                $mitarbeiter = getActiveMitarbeiter($pdo);
                foreach ($mitarbeiter as $person) {
                    echo '<option value="' . $person['MitarbeiterID'] . '">' . htmlspecialchars($person['Nachname'] . ', ' . $person['Vorname']) . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="monat">Monat wählen:</label>
            <input type="month" id="monat" name="monat" value="<?php echo date('Y-m'); ?>" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Dienstplan anzeigen</button>
    </form>
    <?php if (!empty($dienstpläne)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Datum</th>x
                        <th>Beginn</th>
                        <th>Ende</th>
                        <th>Pause (Stunden)</th>
                        <th>Arbeitsstunden (ohne Pause)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $totalHours = 0;
                    foreach ($dienstpläne as $row) {
                        $pause = calculatePause($row["beginn"], $row["ende"]);
                        $arbeitsstunden = ($row["Arbeitsminuten"] / 60) - $pause; // Berechne Arbeitsstunden ohne Pause
                        echo "<tr><td>" . htmlspecialchars($row["datum"]) . "</td><td>" . htmlspecialchars($row["beginn"]) . "</td><td>" . htmlspecialchars($row["ende"]) . "</td><td>" . $pause . "</td><td>" . $arbeitsstunden . "</td></tr>";
                        $totalHours += $arbeitsstunden;
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4">Gesamtstunden (ohne Pause)</th>
                        <th><?= $totalHours ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    <?php endif; ?>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
