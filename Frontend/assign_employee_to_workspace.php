<?php
include '../config/database.php'; // Stellen Sie sicher, dass der Pfad korrekt ist.
$mitarbeiter_id = '';
$arbeitsbereich_id = '';
function fetchOptions($table, $activeOnly = false) {
    $pdo = pdo();
    if ($table === 'mitarbeiter' && $activeOnly) {
        $query = "SELECT MitarbeiterID, Vorname, Nachname FROM $table WHERE Status = 'AKTIV'";
    } else {
        $query = "SELECT ArbeitsbereichID, Name FROM $table";
    }
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$mitarbeiterOptions = fetchOptions('mitarbeiter', true);
$arbeitsbereichOptions = fetchOptions('arbeitsbereiche');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $mitarbeiter_id = $_POST['mitarbeiter_id'];
    $arbeitsbereich_id = $_POST['arbeitsbereich_id'];

    $pdo = pdo();
    $sql = "INSERT INTO mitarbeiter_arbeitsbereich (mitarbeiter_id, arbeitsbereich_id) VALUES (?, ?)";
    $stmt = $pdo->prepare($sql);
    if ($stmt->execute([$mitarbeiter_id, $arbeitsbereich_id])) {
        header("Location: arbeitsbereiche.php"); // Passen Sie diese URL gegebenenfalls an.
    } else {
        echo "Ein Fehler ist aufgetreten.";
    }
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mitarbeiter einem Arbeitsbereich zuweisen</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
        }
        .header {
            background-color: #ff8c00; /* Orange */
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #ff8c00 !important;
            border-color: #ff8c00 !important;
        }
        .form-container {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .form-group {
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<?php include 'navbar.php'; ?>
<h1 class="header">Mitarbeiter einem Arbeitsbereich zuweisen</h1>

<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-6 form-container">
            <form method="post">
                <div class="form-group">
                    <label for="mitarbeiter_id">Mitarbeiter:</label>
                    <select class="form-control" name="mitarbeiter_id">
                        <?php foreach ($mitarbeiterOptions as $option) {
                            echo "<option value='{$option['MitarbeiterID']}'>{$option['Vorname']} {$option['Nachname']}</option>";
                        } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="arbeitsbereich_id">Arbeitsbereich:</label>
                    <select class="form-control" name="arbeitsbereich_id">
                        <?php foreach ($arbeitsbereichOptions as $option) {
                            echo "<option value='{$option['ArbeitsbereichID']}'>{$option['Name']}</option>";
                        } ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Zuweisen</button>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
