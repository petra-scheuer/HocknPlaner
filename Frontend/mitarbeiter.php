<?php
include '../config/database.php';
include 'navbar.php';


function fetchEmployees($status = '', $personalnummer = '', $sort = 'Vorname', $order = 'ASC') {
    $pdo = pdo();

    $sql = "SELECT * FROM mitarbeiter";
    $where = [];
    $params = [];

    if (!empty($status)) {
        if ($status === 'aktiv') {
            $where[] = "Status = 'AKTIV'";
        } elseif ($status === 'inaktiv') {
            $where[] = "Status = 'INAKTIV'";
        }
    }

    if (!empty($personalnummer)) {
        $where[] = "Personalnummer LIKE ?";
        $params[] = "%$personalnummer%";
    }

    if (!empty($where)) {
        $sql .= " WHERE " . implode(" AND ", $where);
    }

    $sql .= " ORDER BY $sort $order";

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Funktion zum Aktualisieren der Mitarbeiterdaten
function updateEmployee($id, $vorname, $nachname, $email, $adresse, $plz, $stadt, $telefon, $geschlecht) {
    $pdo = pdo();
    $stmt = $pdo->prepare("UPDATE mitarbeiter SET vorname = ?, nachname = ?, email = ?, adresse = ?, plz = ?, stadt = ?, telefon = ?, geschlecht = ? WHERE MitarbeiterID = ?");
    $stmt->execute([$vorname, $nachname, $email, $adresse, $plz, $stadt, $telefon, $geschlecht, $id]);
}

// Funktion zum Ändern des Status eines Mitarbeiters
function changeEmployeeStatus($id, $status) {
    $pdo = pdo();
    $stmt = $pdo->prepare("UPDATE mitarbeiter SET Status = ? WHERE MitarbeiterID = ?");
    $stmt->execute([$status, $id]);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $pdo = pdo();
    $stmt = $pdo->prepare("INSERT INTO mitarbeiter (geschlecht, vorname, nachname, telefon, email, adresse, plz, stadt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->execute([$_POST['geschlecht'], $_POST['vorname'], $_POST['nachname'], $_POST['telefon'], $_POST['email'], $_POST['adresse'], $_POST['plz'], $_POST['stadt']]);
    header("Location: mitarbeiter.php");
    exit;
}

$statusFilter = $_GET['status'] ?? '';
$personalnummerFilter = $_GET['personalnummer'] ?? '';

$employees = fetchEmployees($statusFilter, $personalnummerFilter);

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mitarbeiter Verwaltung</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            color: #333;
        }
        .header {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
        .filter-legend {
            margin-top: 10px;
            display: flex;
            justify-content: center;
        }
        .filter-link {
            margin: 0 5px;
        }
    </style>
</head>
<body>

<h1 class="header">Mitarbeiter Verwaltung</h1>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-primary mb-3" onclick="window.location.href='neuer_mitarbeiter.php'">Neuer Mitarbeiter</button>
        </div>
        <div class="col-md-6">
            <form action="mitarbeiter.php" method="get">
                <div class="input-group">
                    <input type="text" name="personalnummer" class="form-control filter-input" placeholder="Personalnummer filtern...">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary">Filtern</button>
                        <a href="#" class="btn btn-secondary">Zurücksetzen</a>
                    </div>
                </div>
                <input type="hidden" name="status" value="<?= htmlspecialchars($statusFilter) ?>">
            </form>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="filter-form">
                <h3>Status filtern: </h3>
                <a class="btn btn-primary filter-link mr-2" href="mitarbeiter.php">Alle</a>
                <a class="btn btn-primary filter-link mr-2" href="mitarbeiter.php?status=aktiv">Aktive</a>
                <a class="btn btn-primary filter-link" href="mitarbeiter.php?status=inaktiv">Inaktive</a>
            </div>
        </div>
    </div>
</div>




    <table class="table table-hover">
        <thead>
            <tr>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Telefon</th>
                <th>Email</th>
                <th>Adresse</th>
                <th>PLZ</th>
                <th>Stadt</th>
                <th>Aktionen</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($employees as $employee) : ?>
                <tr>
                    <td><?= htmlspecialchars($employee['Vorname']) ?></td>
                    <td><?= htmlspecialchars($employee['Nachname']) ?></td>
                    <td><?= htmlspecialchars($employee['telefon']) ?></td>
                    <td><?= htmlspecialchars($employee['email']) ?></td>
                    <td><?= htmlspecialchars($employee['Adresse']) ?></td>
                    <td><?= htmlspecialchars($employee['PLZ']) ?></td>
                    <td><?= htmlspecialchars($employee['Stadt']) ?></td>
                    <td>
                        <a href="mitarbeiterprofil.php?id=<?= $employee['MitarbeiterID'] ?>" class="btn btn-primary">Details ansehen</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="js/filter.js"></script>
</body>
</html>
