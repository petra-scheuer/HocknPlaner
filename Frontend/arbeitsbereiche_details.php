<?php
ob_start(); // Startet Output Buffering am Anfang des Skripts

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include '../config/database.php';
include 'navbar.php';


// Überprüfen, ob die Arbeitsbereichs-ID übergeben wurde
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $arbeitsbereich_id = $_GET['id'];
    
    // Funktion zum Abrufen der Arbeitsbereichsdetails
    function fetchArbeitsbereichDetails($arbeitsbereich_id) {
        $pdo = pdo();
        $stmt = $pdo->prepare("SELECT * FROM arbeitsbereiche WHERE ArbeitsbereichID = :id");
        $stmt->bindParam(':id', $arbeitsbereich_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    // Funktion zum Abrufen der zugeordneten Mitarbeiter
    function fetchZugeordneteMitarbeiter($arbeitsbereich_id) {
        $pdo = pdo();
        $stmt = $pdo->prepare("SELECT m.* FROM mitarbeiter m JOIN mitarbeiter_arbeitsbereich ma ON m.MitarbeiterID = ma.mitarbeiter_id WHERE ma.arbeitsbereich_id = :id");
        $stmt->bindParam(':id', $arbeitsbereich_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    // Funktion zum Abrufen aller Mitarbeiter für das Dropdown-Menü
    function fetchAllMitarbeiter() {
        $pdo = pdo();
        $stmt = $pdo->prepare("SELECT * FROM mitarbeiter");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    // Funktion zum Zuweisen eines Mitarbeiters zu einem Arbeitsbereich
    function assignMitarbeiterToArbeitsbereich($mitarbeiter_id, $arbeitsbereich_id) {
        $pdo = pdo();
        $stmt = $pdo->prepare("INSERT INTO mitarbeiter_arbeitsbereich (mitarbeiter_id, arbeitsbereich_id) VALUES (:mitarbeiter_id, :arbeitsbereich_id)");
        $stmt->bindParam(':mitarbeiter_id', $mitarbeiter_id, PDO::PARAM_INT);
        $stmt->bindParam(':arbeitsbereich_id', $arbeitsbereich_id, PDO::PARAM_INT);
        $stmt->execute();
    }
    
    // Funktion zum Entfernen eines Mitarbeiters aus einem Arbeitsbereich
    function removeMitarbeiterFromArbeitsbereich($mitarbeiter_id, $arbeitsbereich_id) {
        $pdo = pdo();
        $stmt = $pdo->prepare("DELETE FROM mitarbeiter_arbeitsbereich WHERE mitarbeiter_id = :mitarbeiter_id AND arbeitsbereich_id = :arbeitsbereich_id");
        $stmt->bindParam(':mitarbeiter_id', $mitarbeiter_id, PDO::PARAM_INT);
        $stmt->bindParam(':arbeitsbereich_id', $arbeitsbereich_id, PDO::PARAM_INT);
        $stmt->execute();
    }
    
    $arbeitsbereich = fetchArbeitsbereichDetails($arbeitsbereich_id);
    $zugeordnete_mitarbeiter = fetchZugeordneteMitarbeiter($arbeitsbereich_id);
    $alle_mitarbeiter = fetchAllMitarbeiter();
    
    // Überprüfen, ob das Formular zum Zuweisen von Mitarbeitern gesendet wurde
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if(isset($_POST['mitarbeiter_id']) && !empty($_POST['mitarbeiter_id'])) {
            $mitarbeiter_id = $_POST['mitarbeiter_id'];
            assignMitarbeiterToArbeitsbereich($mitarbeiter_id, $arbeitsbereich_id);
            // Seite neu laden, um die aktualisierten Daten anzuzeigen
            header("Refresh:0");
            exit();
        }
    }
    
    // Überprüfen, ob eine Aktion zum Entfernen eines Mitarbeiters aus dem Arbeitsbereich gesendet wurde
    if (isset($_GET['remove']) && isset($_GET['mitarbeiter_id'])) {
        $mitarbeiter_id = $_GET['mitarbeiter_id'];
        removeMitarbeiterFromArbeitsbereich($mitarbeiter_id, $arbeitsbereich_id);
        // Seite neu laden, um die aktualisierten Daten anzuzeigen
        header("Location: arbeitsbereiche_details.php?id=$arbeitsbereich_id");
        exit();
    }
} else {
    // Fehlermeldung anzeigen, wenn keine Arbeitsbereichs-ID übergeben wurde
    echo "Arbeitsbereichs-ID nicht vorhanden";
    exit();
}
ob_end_flush();

?>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arbeitsbereichdetails</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
       
       	header, .btn-primary, th { background-color: #6B8E23; color: #6B8E23; }
        .card-custom { box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 10px; }
        .main-title { font-size: 30px; font-weight: bold; color: #6B8E23; }
        .detail-title { font-size: 18px; font-weight: bold; color: #7; }
        .error-message { color: red; }
        .dropdown-menu { max-height: 200px; overflow-y: auto; }
        .remove-btn { float: right; }
    </style>
</head>
<body>
<div class="container mt-5">
    <h2 class="main-title">Arbeitsbereichdetails - <?= isset($arbeitsbereich['Name']) ? htmlspecialchars($arbeitsbereich['Name']) : '' ?></h2>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-custom p-3">
                <div class="detail-section">
                    <p class="detail-title">Arbeitsbereichsinformationen</p>
                    <table class="table">
                        <tr><td>Name:</td><td><?= isset($arbeitsbereich['Name']) ? htmlspecialchars($arbeitsbereich['Name']) : '' ?></td></tr>
                        <tr><td>Typ:</td><td><?= isset($arbeitsbereich['TYP']) ? htmlspecialchars($arbeitsbereich['TYP']) : '' ?></td></tr>
                        <tr><td>Adresse:</td><td><?= isset($arbeitsbereich['Adresse']) ? htmlspecialchars($arbeitsbereich['Adresse']) : '' ?></td></tr>
                        <tr><td>PLZ:</td><td><?= isset($arbeitsbereich['PLZ']) ? htmlspecialchars($arbeitsbereich['PLZ']) : '' ?></td></tr>
                        <tr><td>Kontakttelefonnummer:</td><td><?= isset($arbeitsbereich['Kontakttelefonnummer']) ? htmlspecialchars($arbeitsbereich['Kontakttelefonnummer']) : '' ?></td></tr>
                        <tr><td>Kontaktperson:</td><td><?= isset($arbeitsbereich['Kontaktperson']) ? htmlspecialchars($arbeitsbereich['Kontaktperson']) : '' ?></td></tr>
                        <tr><td>Informationen:</td><td><?= isset($arbeitsbereich['Informationen']) ? htmlspecialchars($arbeitsbereich['Informationen']) : '' ?></td></tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-custom p-3">
                <div class="detail-section">
                    <p class="detail-title">Mitarbeiter zuweisen</p>
                    <form method="post">
                        <label for="mitarbeiter_id">Mitarbeiter auswählen:</label>
                        <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                            <?php foreach ($alle_mitarbeiter as $mitarbeiter): ?>
                                <option value="<?= $mitarbeiter['MitarbeiterID'] ?>">
                                    <?= isset($mitarbeiter['Vorname']) && isset($mitarbeiter['Nachname']) ? htmlspecialchars($mitarbeiter['Vorname'] . ' ' . $mitarbeiter['Nachname']) : '' ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <button type="submit" class="btn btn-primary mt-2">Mitarbeiter zuweisen</button>
                    </form>
                </div>
                <div class="detail-section mt-3">
                    <p class="detail-title">Zugeordnete Mitarbeiter</p>
                    <table class="table table-bordered">
                        <?php foreach ($zugeordnete_mitarbeiter as $mitarbeiter): ?>
                            <tr>
                                <td><?= isset($mitarbeiter['Vorname']) && isset($mitarbeiter['Nachname']) ? htmlspecialchars($mitarbeiter['Vorname'] . ' ' . $mitarbeiter['Nachname']) : '' ?></td>
                                <td><a href="?id=<?= $arbeitsbereich_id ?>&remove=true&mitarbeiter_id=<?= $mitarbeiter['MitarbeiterID'] ?>" class="remove-btn">Entfernen</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <a href="arbeitsbereiche.php" class="btn btn-primary mt-2">Zurück zur Übersicht</a>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
