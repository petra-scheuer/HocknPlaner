<?php include 'navbar.php'; ?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Neuen Mitarbeiter anlegen</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: ;
            color: #333;
        }
        .header {
            background-color: #6B8E23;; /* Orange */
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23; !important;
            border-color: #6B8E23; !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;; /* Orange */
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
    </style>
</head>
<body>
<h1 class="header">Neuen Mitarbeiter anlegen</h1>


<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <form class="col-md-6" action="mitarbeiter.php" method="post">
           



            <div class="form-group">
                <label for="geschlecht">Geschlecht:</label>
                <select class="form-control" id="geschlecht" name="geschlecht">
                    <option value="Frau">Frau</option>
                    <option value="Mann">Mann</option>
                </select>
            </div>
            <div class="form-group">
                <label for="vorname">Vorname:</label>
                <input type="text" class="form-control" id="vorname" name="vorname" required>
            </div>
            <div class="form-group">
                <label for="nachname">Nachname:</label>
                <input type="text" class="form-control" id="nachname" name="nachname" required>
            </div>
            <div class="form-group">
                <label for="telefon">Telefonnummer:</label>
                <input type="text" class="form-control" id="telefon" name="telefon" required>
            </div>
            <div class="form-group">
                <label for="email">Emailadresse:</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="adresse">Adresse:</label>
                <input type="text" class="form-control" id="adresse" name="adresse" required>
            </div>
            <div class="form-group">
                <label for="plz">PLZ:</label>
                <input type="text" class="form-control" id="plz" name="plz" required>
            </div>
            <div class="form-group">
                <label for="stadt">Stadt:</label>
                <input type="text" class="form-control" id="stadt" name="stadt" required>
            </div>
            <button type="submit" class="btn btn-primary">Mitarbeiter Anlegen</button>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
