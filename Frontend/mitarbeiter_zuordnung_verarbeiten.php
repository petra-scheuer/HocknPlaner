<?php
include '../config/database.php';

// Verbessertes Handling der Eingaben
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && !empty($_POST['dienst_id'])) {
    $pdo = pdo();

    $dienst_id = intval($_POST['dienst_id']);
    $action = $_POST['action'];
    $mitarbeiter_ids = isset($_POST['mitarbeiter_ids']) ? $_POST['mitarbeiter_ids'] : [];

    if (!empty($mitarbeiter_ids)) {
        if ($action === 'assign') {
            // Assign selected employees to the service
            $stmt = $pdo->prepare("INSERT INTO dienst_mitarbeiter (dienst_id, mitarbeiter_id) VALUES (:dienst_id, :mitarbeiter_id)");
            foreach ($mitarbeiter_ids as $mitarbeiter_id) {
                $stmt->execute(['dienst_id' => $dienst_id, 'mitarbeiter_id' => $mitarbeiter_id]);
            }
        } elseif ($action === 'remove') {
            // Sicherheit verbessern durch Prepared Statements statt imploden
            $placeholders = implode(',', array_fill(0, count($mitarbeiter_ids), '?'));
            $stmt = $pdo->prepare("DELETE FROM dienst_mitarbeiter WHERE dienst_id = :dienst_id AND mitarbeiter_id IN ($placeholders)");
            $stmt->execute(array_merge([$dienst_id], $mitarbeiter_ids));
        }

        // Redirect back to service details page with the current service ID
        header("Location: dienst_details.php?id=$dienst_id");
        exit;
    } else {
        echo "Keine Mitarbeiter ausgewählt.";
    }
} else {
    echo "Formulardaten nicht korrekt empfangen oder unvollständig.";
}
?>
