<?php
ob_start(); // Startet Output Buffering am Anfang des Skripts

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include '../config/database.php';
include 'navbar.php';


// Funktion, um alle Arbeitsbereiche aus der Datenbank zu holen
function getAlleArbeitsbereiche() {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT * FROM arbeitsbereiche");
    $stmt->execute();
    return $stmt->fetchAll();
}

// Funktion zum Hinzufügen eines neuen Dienstes
function addNeuerDienst($arbeitsbereich_id, $datum, $beginn, $ende, $status, $anzahl) {
    $pdo = pdo();
    $stmt = $pdo->prepare("INSERT INTO termine (arbeitsbereich_id, datum, beginn, ende, status, anzahl) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->execute([$arbeitsbereich_id, $datum, $beginn, $ende, $status, $anzahl]);
}


// Überprüfen, ob das Formular gesendet wurde
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Variablen aus dem Formular abrufen
    $arbeitsbereich_id = $_POST["arbeitsbereich_id"];
    $datum = $_POST["datum"];
    $beginn = $_POST["beginn"];
    $ende = $_POST["ende"];
    $status = $_POST["status"];
    $anzahl = $_POST["anzahl"];

    // Neuen Dienst hinzufügen
    addNeuerDienst($arbeitsbereich_id, $datum, $beginn, $ende, $status, $anzahl);

    // Weiterleitung zur Dienstplanseite
    header("Location: dienstplan.php");
    exit;
  ob_end_flush();

}

// Alle Arbeitsbereiche abrufen
$arbeitsbereiche = getAlleArbeitsbereiche();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Neuer Dienst</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        body {
            color: #333;
        }
        .header {
            background-color: #6B8E23;; /* Orange */
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23; !important;
            border-color: #6B8E23; !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;; /* Orange */
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
    </style>
</head>
<body>
<h1 class="header">Neuen Dienst anlegen</h1>


<div class="container">
    <form method="post">
        <div class="form-group">
            <label for="arbeitsbereich_id">Arbeitsbereich</label>
            <select class="form-control" id="arbeitsbereich_id" name="arbeitsbereich_id">
                <?php foreach ($arbeitsbereiche as $arbeitsbereich): ?>
                    <option value="<?= $arbeitsbereich['ArbeitsbereichID'] ?>"><?= $arbeitsbereich['Name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="datum">Datum</label>
            <input type="date" class="form-control" id="datum" name="datum" required>
        </div>
        <div class="form-group">
            <label for="beginn">Beginn</label>
            <input type="time" class="form-control" id="beginn" name="beginn" required>
        </div>
        <div class="form-group">
            <label for="ende">Ende</label>
            <input type="time" class="form-control" id="ende" name="ende" required>
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control" id="status" name="status" required>
                <option value="FORECAST">Forecast</option>
                <option value="OFFEN">Offen</option>
                <option value="BESETZT">Besetzt</option>
                <option value="FERTIG">Fertig</option>
            </select>
        </div>
        <div class="form-group">
            <label for="anzahl">Anzahl benötigter Mitarbeiter</label>
            <input type="number" class="form-control" id="anzahl" name="anzahl" required>
        </div>
        <button type="submit" class="btn btn-primary">Dienst hinzufügen</button>
    </form>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
</body>
</html>
