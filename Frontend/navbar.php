<?php
session_start();

// Überprüfen, ob der Benutzer nicht eingeloggt ist
if (!isset($_SESSION['loggedin'])) {
    // Wenn nicht, leite ihn zur Login-Seite weiter
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Startseite - Schichtplaner</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .navbar-custom {
            background-color: #6B8E23  ; /* Dunkleres Grün für die Navbar */
        }
        .navbar-custom .navbar-nav .nav-link {
            color: white; /* Weiße Schriftfarbe für bessere Sichtbarkeit */
        }
        .navbar-custom .navbar-nav .nav-link:hover,
        .navbar-custom .navbar-nav .nav-link:focus {
            color: #A5D6A7; /* Aufgehelltes Grün für Hover- und Fokus-Effekte */
        }
        .navbar-custom .navbar-brand {
            color: white; /* Weiße Schriftfarbe für den Markennamen */
            display: flex;
            align-items: center;
        }
        .navbar-custom .navbar-brand img {
            height: 60px; /* Vergrößerte Höhe des Logos */
            margin-right: 10px; /* Abstand zwischen Logo und Text */
        }
        .navbar-custom .navbar-toggler {
            border-color: #A5D6A7; /* Angepasstes helleres Grün für den Umschaltknopf */
        }

        .navbar-custom .navbar-toggler-icon {
            background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3e%3cpath stroke='rgba(165, 214, 167, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e");
            /* Benutzerdefinierte SVG für das Toggler-Icon */
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-custom">
        <a class="navbar-brand" href="index.php">
		<img src="Logo.png" alt="Logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" href="mitarbeiter.php">Mitarbeiter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="dienstplan.php">Dienstplan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Mitarbeiter_Dienstplan.php">Mitarbeiter Dienstplan</a>
                </li>
                <li class="nav-item"> 
                    <a class="nav-link" href="arbeitsbereiche.php">Arbeitsbereiche</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="fehlzeiten.php">Fehlzeiten</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auswertungen.php">Auswertungen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nachbearbeitung.php">Nachbearbeitung</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php">Admin</a>
                </li>
                <!-- Ausloggen-Button -->
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Ausloggen</a>
                </li>
            </ul>
        </div>
    </nav>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
