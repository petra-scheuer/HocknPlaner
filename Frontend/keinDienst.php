<?php
// Datenbankkonfiguration einbinden
include '../config/database.php';
include 'navbar.php';

$pdo = pdo(); // Funktion aufrufen, um die PDO-Instanz zu bekommen

// Aktuelles Datum ermitteln
$aktuellesJahr = date('Y');
$aktuellerMonat = date('m');

// SQL-Abfrage vorbereiten
$sql = "SELECT m.MitarbeiterID, m.Vorname, m.Nachname
        FROM mitarbeiter m
        WHERE NOT EXISTS (
            SELECT 1 FROM termine t
            INNER JOIN dienst_mitarbeiter dm ON dm.dienst_id = t.termin_id
            WHERE dm.mitarbeiter_id = m.MitarbeiterID AND 
                  MONTH(t.datum) = :aktuellerMonat AND YEAR(t.datum) = :aktuellesJahr
        )";

$stmt = $pdo->prepare($sql);
$stmt->bindParam(':aktuellerMonat', $aktuellerMonat, PDO::PARAM_INT);
$stmt->bindParam(':aktuellesJahr', $aktuellesJahr, PDO::PARAM_INT);
$stmt->execute();

// Ergebnisse in ein Array speichern
$mitarbeiterOhneEinsatz = $stmt->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html lang="de">
<style>
        body { background-color: #f4f4f4; color: #333; }
        .header { background-color: #6B8E23; color: #ffffff; padding: 10px 0; text-align: center; }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table { background-color: #ffffff; margin-top: 20px; }
 		.btn-primary, th { background-color: #6B8E23; color: #6B8E23; }
        .card-custom { box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 10px; }
        .main-title { font-size: 30px; font-weight: bold; color: #6B8E23; }
        .detail-title { font-size: 18px; font-weight: bold; color: #7; }
        .error-message { color: red; }
        .dropdown-menu { max-height: 200px; overflow-y: auto; }
        .remove-btn { float: right; }
        th { background-color: #6B8E23; color: #ffffff; }
    </style>
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter ohne Einsätze diesen Monat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2 class="main-title">Mitarbeiter ohne Einsätze diesen Monat</h2>
        <?php if (count($mitarbeiterOhneEinsatz) > 0): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>MitarbeiterID</th>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Profil</th> <!-- Additional header for profile link -->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($mitarbeiterOhneEinsatz as $mitarbeiter): ?>
                        <tr>
                            <td><?= htmlspecialchars($mitarbeiter['MitarbeiterID']) ?></td>
                            <td><?= htmlspecialchars($mitarbeiter['Vorname']) ?></td>
                            <td><?= htmlspecialchars($mitarbeiter['Nachname']) ?></td>
                            <td><a href="mitarbeiterprofil.php?id=<?= $mitarbeiter['MitarbeiterID'] ?>">Profil ansehen</a></td> <!-- Link to the profile page -->
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p>Alle Mitarbeiter haben diesen Monat Einsätze.</p>
        <?php endif; ?>
    </div>

    <!-- JavaScript-Bibliotheken einfügen -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
