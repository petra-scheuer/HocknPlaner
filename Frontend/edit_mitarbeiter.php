<?php
ob_start(); // Startet Output Buffering am Anfang des Skripts

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../config/database.php'; // Pfad zur Datenbankkonfigurationsdatei
include 'navbar.php'; // Navigationsleiste

$pdo = pdo();

$id = $_GET['id'] ?? null; // Null-coalescing Operator für den Fall, dass 'id' nicht gesetzt ist
$employee = null;

if ($id) {
    $stmt = $pdo->prepare("SELECT * FROM mitarbeiter WHERE MitarbeiterID = ?");
    $stmt->execute([$id]);
    $employee = $stmt->fetch(PDO::FETCH_ASSOC);
}

if (!$employee) {
    echo "Kein Mitarbeiter mit der angegebenen ID gefunden.";
    exit; // Stoppt die Ausführung weiterer Code-Teile, wenn kein Mitarbeiter gefunden wird
}

// Funktion zum Aktualisieren der Mitarbeiterdaten
function updateEmployee($id, $vorname, $nachname, $email, $adresse, $plz, $stadt, $telefon, $geschlecht, $status, $geburtstag, $einrittsdatum, $vertragsstundensoll, $personalnummer) {
    $pdo = pdo();
    $sql = "UPDATE mitarbeiter SET Vorname = ?, Nachname = ?, Email = ?, Adresse = ?, PLZ = ?, Stadt = ?, Telefon = ?, Geschlecht = ?, Status = ?, Geburtstag = ?, Eintrittsdatum = ?, Vertragsstundensoll = ?, Personalnummer = ? WHERE MitarbeiterID = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$vorname, $nachname, $email, $adresse, $plz, $stadt, $telefon, $geschlecht, $status, $geburtstag, $einrittsdatum, $vertragsstundensoll, $personalnummer, $id]);
}

// Funktion zum Deaktivieren eines Mitarbeiters
function deactivateEmployee($mitarbeiterId, $pdo) {
    try {
        $pdo->beginTransaction();
        $stmt = $pdo->prepare("UPDATE mitarbeiter SET Status = 'inaktiv' WHERE MitarbeiterID = ?");
        $stmt->execute([$mitarbeiterId]);
        $pdo->commit();
    } catch (PDOException $e) {
        $pdo->rollBack();
        throw $e;
    }
}

// Formularhandling
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['mitarbeiterID']; // Die Mitarbeiter-ID aus dem Formular
    if ($_POST['action'] == 'save') {
        updateEmployee(
            $id,
            $_POST['vorname'],
            $_POST['nachname'],
            $_POST['email'],
            $_POST['adresse'],
            $_POST['plz'],
            $_POST['stadt'],
            $_POST['telefon'],
            $_POST['geschlecht'],
            $_POST['status'],
            $_POST['geburtstag'],
            $_POST['einrittsdatum'],
            $_POST['vertragsstundensoll'],
            $_POST['personalnummer']
        );
        header("Location: mitarbeiter.php");
        exit;
    } elseif ($_POST['action'] == 'deactivate') {
        deactivateEmployee($id, $pdo);
        header("Location: mitarbeiter.php");
        exit;
    }
}

ob_end_flush();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mitarbeiter Verwaltung</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            color: #333;
        }
        .header {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23;
            border-color: #6B8E23;
        }
        .btn-danger, .btn-danger:hover, .btn-danger:active, .btn-danger:visited {
            background-color: #6B8E23;
            border-color: #6B8E23;
        }
    </style>
</head>
<body>
<h1 class="header">Stammdaten ändern</h1>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form method="post">
                <div class="form-group">
                    <label for="mitarbeiterID">MitarbeiterID:</label>
                    <input type="text" class="form-control" id="mitarbeiterID" name="mitarbeiterID" value="<?= htmlspecialchars($employee['MitarbeiterID']) ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="personalnummer">Personalnummer:</label>
                    <input type="text" class="form-control" id="personalnummer" name="personalnummer" value="<?= htmlspecialchars($employee['Personalnummer']) ?>">
                </div>
                <div class="form-group">
                    <label for="vorname">Vorname:</label>
                    <input type="text" class="form-control" id="vorname" name="vorname" value="<?= htmlspecialchars($employee['Vorname']) ?>">
                </div>
                <div class="form-group">
                    <label for="nachname">Nachname:</label>
                    <input type="text" class="form-control" id="nachname" name="nachname" value="<?= htmlspecialchars($employee['Nachname']) ?>">
                </div>
                <div class="form-group">
                    <label for="telefon">Telefon:</label>
                    <input type="text" class="form-control" id="telefon" name="telefon" value="<?= htmlspecialchars($employee['telefon']) ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" id="email" name="email" value="<?= htmlspecialchars($employee['email']) ?>">
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse:</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" value="<?= htmlspecialchars($employee['Adresse']) ?>">
                </div>
                <div class="form-group">
                    <label for="plz">PLZ:</label>
                    <input type="text" class="form-control" id="plz" name="plz" value="<?= htmlspecialchars($employee['PLZ']) ?>">
                </div>
                <div class="form-group">
                    <label for="stadt">Stadt:</label>
                    <input type="text" class="form-control" id="stadt" name="stadt" value="<?= htmlspecialchars($employee['Stadt']) ?>">
                </div>
                <div class="form-group">
                    <label for="status">Status:</label>
                    <select class="form-control" id="status" name="status">
                        <option value="aktiv" <?= $employee['Status'] == 'aktiv' ? 'selected' : '' ?>>Aktiv</option>
                        <option value="inaktiv" <?= $employee['Status'] == 'inaktiv' ? 'selected' : '' ?>>Inaktiv</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="geburtstag">Geburtstag:</label>
                    <input type="date" class="form-control" id="geburtstag" name="geburtstag" value="<?= $employee['Geburtstag'] ?>">
                </div>
                <div class="form-group">
                    <label for="einrittsdatum">Eintrittsdatum:</label>
                    <input type="date" class="form-control" id="einrittsdatum" name="einrittsdatum" value="<?= $employee['Eintrittsdatum'] ?>">
                </div>
                <div class="form-group">
                    <label for="vertragsstundensoll">Vertragsstundensoll:</label>
                    <input type="number" class="form-control" id="vertragsstundensoll" name="vertragsstundensoll" value="<?= $employee['Vertragsstundensoll'] ?>">
                </div>
                <button type="submit" name="action" value="save" class="btn btn-primary">Speichern</button>
                <button type="submit" name="action" value="deactivate" class="btn btn-danger" onclick="return confirm('Sind Sie sicher, dass Sie diesen Mitarbeiter deaktivieren möchten?');">Mitarbeiter deaktivieren</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
