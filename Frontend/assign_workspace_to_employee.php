<?php
include '../config/database.php';

function fetchAllEmployees() {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT MitarbeiterID, Vorname, Nachname FROM mitarbeiter WHERE Status = 'AKTIV'");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function fetchAllArbeitsbereiche() {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT ArbeitsbereichID, Name FROM arbeitsbereiche");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$mitarbeiter = fetchAllEmployees();
$arbeitsbereiche = fetchAllArbeitsbereiche();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $mitarbeiter_id = $_POST['mitarbeiter_id'];
    $arbeitsbereich_ids = $_POST['arbeitsbereich_ids'] ?? [];

    $pdo = pdo();
    foreach ($arbeitsbereich_ids as $arbeitsbereich_id) {
        $sql = "INSERT INTO mitarbeiter_arbeitsbereich (mitarbeiter_id, arbeitsbereich_id) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$mitarbeiter_id, $arbeitsbereich_id]);
    }

    header("Location: arbeitsbereiche.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mitarbeiter Arbeitsbereiche Zuweisen</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Arbeitsbereiche einem Mitarbeiter zuweisen</h1>
    <form action="" method="post">
        <div class="form-group">
            <label for="mitarbeiter_id">Mitarbeiter auswählen:</label>
            <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                <?php foreach ($mitarbeiter as $m) : ?>
                    <option value="<?= $m['MitarbeiterID'] ?>"><?= htmlspecialchars($m['Vorname'] . ' ' . $m['Nachname']) ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label>Arbeitsbereiche zuweisen:</label>
            <?php foreach ($arbeitsbereiche as $ab) : ?>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="arbeitsbereich_ids[]" value="<?= $ab['ArbeitsbereichID'] ?>" id="ab<?= $ab['ArbeitsbereichID'] ?>">
                    <label class="form-check-label" for="ab<?= $ab['ArbeitsbereichID'] ?>"><?= htmlspecialchars($ab['Name']) ?></label>
                </div>
            <?php endforeach; ?>
        </div>
        <button type="submit" class="btn btn-primary">Zuweisen</button>
    </form>
</div>
</body>
</html>
