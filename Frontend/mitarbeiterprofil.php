

<?php
ob_start(); // Startet Output Buffering am Anfang des Skripts

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include '../config/database.php';


function fetchEmployeeDetails($mitarbeiter_id) {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT m.*, a.Name as Arbeitsbereich FROM mitarbeiter m 
                            LEFT JOIN mitarbeiter_arbeitsbereich ma ON m.MitarbeiterID = ma.mitarbeiter_id
                            LEFT JOIN arbeitsbereiche a ON ma.arbeitsbereich_id = a.ArbeitsbereichID
                            WHERE m.MitarbeiterID = ?");
    $stmt->execute([$mitarbeiter_id]);
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

$employee = null;

if (isset($_GET['id']) && $_GET['id'] != '') {
    $employee = fetchEmployeeDetails($_GET['id']);
}

if (!$employee) {
    header("Location: mitarbeiter.php");
    exit;
}

function calculateWorkDays($start, $end) {
    $startDate = new DateTime($start);
    $endDate = new DateTime($end);
    $workDays = 0;
    while ($startDate <= $endDate) {
        if ($startDate->format('N') < 6) { // 'N' gibt den Wochentag zurück, wobei Montag = 1 und Sonntag = 7
            $workDays++;
        }
        $startDate->modify('+1 day');
    }
    return $workDays;
}

function getMonthlyWorkHours($einDatum, $ausDatum, $vertragsStunden) {
    $aktuellerMonatStart = date('Y-m-01'); // Erster Tag des aktuellen Monats
    $aktuellerMonatEnde = date('Y-m-t'); // Letzter Tag des aktuellen Monats

    $startDatum = max($aktuellerMonatStart, $einDatum);
    $endDatum = ($ausDatum !== null && $ausDatum < $aktuellerMonatEnde) ? $ausDatum : $aktuellerMonatEnde;

    $arbeitstage = calculateWorkDays($startDatum, $endDatum);
    $monatlicheArbeitsstunden = ($vertragsStunden / 5) * $arbeitstage; // Teile durch 5 Arbeitstage pro Woche

    return $monatlicheArbeitsstunden;
}

$monatlicheArbeitsstunden = getMonthlyWorkHours($employee['Eintrittsdatum'], $employee['Austrittsdatum'], $employee['Vertragsstundensoll']);


function getActiveMitarbeiter($pdo) {
    $sql = "SELECT MitarbeiterID, Vorname, Nachname FROM mitarbeiter WHERE Status = 'aktiv' ORDER BY Nachname, Vorname";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getDienstplan($pdo, $mitarbeiterId, $monatJahr) {
    $startDatum = $monatJahr . '-01'; // Erster Tag des gewählten Monats
    $endDatum = date('Y-m-t', strtotime($startDatum)); // Letzter Tag des gewählten Monats
    $sql = "SELECT t.datum, t.beginn, t.ende, TIMESTAMPDIFF(MINUTE, t.beginn, t.ende) as Arbeitsminuten FROM termine t
            INNER JOIN dienst_mitarbeiter dm ON dm.dienst_id = t.termin_id
            WHERE dm.mitarbeiter_id = :mitarbeiterId AND t.datum BETWEEN :startDatum AND :endDatum";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':mitarbeiterId', $mitarbeiterId, PDO::PARAM_INT);
    $stmt->bindParam(':startDatum', $startDatum);
    $stmt->bindParam(':endDatum', $endDatum);
    $stmt->execute();
    return $stmt->fetchAll();
}

// Funktion zur Berechnung der Pause basierend auf der Dauer des Dienstes
function calculatePause($start, $end) {
    $startTime = new DateTime($start);
    $endTime = new DateTime($end);
    $duration = $startTime->diff($endTime)->h + ($startTime->diff($endTime)->i / 60);

    if ($duration <= 6) {
        return 0;
    } elseif ($duration <= 8) {
        return 0.5;
    } else {
        return 1;
    }
}

$dienstpläne = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['mitarbeiter_id'], $_POST['monat'])) {
    $mitarbeiterId = (int)$_POST['mitarbeiter_id'];
    $monatJahr = $_POST['monat']; // Format yyyy-mm
    $dienstpläne = getDienstplan($pdo, $mitarbeiterId, $monatJahr);
}
ob_end_flush();

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mitarbeiterdetails</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
            font-family: Arial, sans-serif;
        }
        .header, .btn-primary, th {
            background-color: #6B8E23;
            color: #ffffff;
        }
        .profile-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            object-fit: cover;
            margin-bottom: 20px;
        }
        .card-custom {
            box-shadow: 0 0 15px rgba(0,0,0,0.2);
            border-radius: 10px;
        }
        .rating {
            unicode-bidi: bidi-override;
            direction: rtl;
            font-size: 1.5em;
        }
        .rating > span {
            cursor: pointer;
        }
        .rating > span:hover,
        .rating > span:hover ~ span {
            color: gold;
        }
        .detail-section {
            margin-bottom: 20px;
        }
        .detail-title {
            font-size: 18px;
            font-weight: bold;
            color: #6B8E23;
        }
        .main-title {
            font-size: 30px;
            font-weight: bold;
            color: #6B8E23;
        }
    </style>
</head>
<body>

<div class="container mt-5">
    <?php if ($employee): ?>
    <h2 class="main-title"><?= htmlspecialchars($employee['Personalnummer']) . " - " . htmlspecialchars($employee['Vorname']) . " " . htmlspecialchars($employee['Nachname']) ?></h2>
    <div class="card card-custom p-3">
        <div class="detail-section">
            <p class="detail-title">Persönliche Informationen</p>
            <table class="info-table">
               <tr><td>Email:</td><td><?= htmlspecialchars($employee['email'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Telefon:</td><td><?= htmlspecialchars($employee['telefon'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Adresse:</td><td><?= htmlspecialchars($employee['Adresse'] ?? 'Keine Angabe') ?><br><?= htmlspecialchars(($employee['PLZ'] ?? '') . ' ' . ($employee['Stadt'] ?? '')) ?></td></tr>
<tr><td>Geburtstag:</td><td><?= htmlspecialchars($employee['Geburtstag'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Eintrittsdatum:</td><td><?= htmlspecialchars($employee['Eintrittsdatum'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Austrittsdatum:</td><td><?= htmlspecialchars($employee['Austrittsdatum'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Status:</td><td><?= htmlspecialchars($employee['Status'] ?? 'Keine Angabe') ?></td></tr>
<tr><td>Vertragsstundensoll:</td><td><?= htmlspecialchars($employee['Vertragsstundensoll'] ?? 'Keine Angabe') ?> Stunden pro Woche</td></tr>
<tr><td>Sollstunden:</td><td><?= number_format($monatlicheArbeitsstunden, 2) ?> Stunden</td></tr>


            </table>
        </div>

<?php $arbeitsbereiche = !empty($employee['Arbeitsbereich']) ? explode(',', $employee['Arbeitsbereich']) : []; ?>

        <div class="detail-section">
            <p class="detail-title">Arbeitsbereiche</p>
            <?php if (!empty($arbeitsbereiche)): ?>
                <ul>
                    <?php foreach ($arbeitsbereiche as $bereich): ?>
                        <li><?= htmlspecialchars(trim($bereich)) ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <p>Keine Arbeitsbereiche zugewiesen.</p>
            <?php endif; ?>
        </div>
        <!-- Button für Mitarbeiter ändern -->
        <a href="edit_mitarbeiter.php?id=<?= $employee['MitarbeiterID'] ?>" class="btn btn-primary">Mitarbeiter ändern</a>
    </div>
  <div class="container mt-3">
    <a href="mitarbeiter.php" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Zurück</a>
</div>


    <?php else: ?>
    <p>Mitarbeiterdetails konnten nicht geladen werden.</p>
    <?php endif; ?>
</div>

<!-- Dienstpläne des Mitarbeiters -->
<div class="container mt-5">
    <div class="row">
        <div class="col">
            <h2>Dienstpläne des Mitarbeiters im aktuellen Monat</h2>
            <form action="" method="post">
                <div class="form-group">
                    <label for="mitarbeiter_id">Mitarbeiter wählen:</label>
                    <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                        <?php
                        $mitarbeiter = getActiveMitarbeiter($pdo);
                        foreach ($mitarbeiter as $person) {
                            echo '<option value="' . $person['MitarbeiterID'] . '">' . htmlspecialchars($person['Nachname'] . ', ' . $person['Vorname']) . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="monat">Monat wählen:</label>
                    <input type="month" id="monat" name="monat" value="<?php echo date('Y-m'); ?>" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Dienstpläne anzeigen</button>
            </form>
            <?php if (!empty($dienstpläne)): ?>
                <div class="table-responsive mt-3">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Datum</th>
                                <th>Beginn</th>
                                <th>Ende</th>
                                <th>Pause (Stunden)</th>
                                <th>Arbeitsstunden (ohne Pause)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $totalHours = 0;
                            foreach ($dienstpläne as $row) {
                                $pause = calculatePause($row["beginn"], $row["ende"]);
                                $arbeitsstunden = ($row["Arbeitsminuten"] / 60) - $pause; // Berechne Arbeitsstunden ohne Pause
                                echo "<tr><td>" . htmlspecialchars($row["datum"]) . "</td><td>" . htmlspecialchars($row["beginn"]) . "</td><td>" . htmlspecialchars($row["ende"]) . "</td><td>" . $pause . "</td><td>" . $arbeitsstunden . "</td></tr>";
                                $totalHours += $arbeitsstunden;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Gesamtstunden (ohne Pause)</th>
                                <th><?= $totalHours ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

</body>
</html>