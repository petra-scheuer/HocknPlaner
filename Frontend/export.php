<?php
include '../config/database.php';

function exportToCSV($start_date, $end_date, $status = '', $arbeitsbereich = '') {
    $pdo = pdo();
    $sql = "
    SELECT t.termin_id, t.datum, t.beginn, t.ende, t.status, a.Name AS ArbeitsbereichName, GROUP_CONCAT(m.Vorname, ' ', m.Nachname) AS EingeteilteMitarbeiter
    FROM termine t
    LEFT JOIN arbeitsbereiche a ON t.arbeitsbereich_id = a.ArbeitsbereichID
    LEFT JOIN dienst_mitarbeiter dm ON t.termin_id = dm.dienst_id
    LEFT JOIN mitarbeiter m ON dm.mitarbeiter_id = m.mitarbeiterID
    WHERE t.datum BETWEEN :start_date AND :end_date" .
    (!empty($status) ? " AND t.status = :status" : "") .
    (!empty($arbeitsbereich) ? " AND a.Name = :arbeitsbereich" : "") .
    " GROUP BY t.termin_id
    ORDER BY t.datum";

    $stmt = $pdo->prepare($sql);
    $params = ['start_date' => $start_date, 'end_date' => $end_date];
    if (!empty($status)) {
        $params['status'] = $status;
    }
    if (!empty($arbeitsbereich)) {
        $params['arbeitsbereich'] = $arbeitsbereich;
    }
    $stmt->execute($params);
    $results = $stmt->fetchAll();

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="dienste.csv"');

    $output = fopen('php://output', 'w');
    fputcsv($output, ['Termin ID', 'Datum', 'Beginn', 'Ende', 'Status', 'Arbeitsbereich', 'Eingeteilte Mitarbeiter'], ';');

    foreach ($results as $row) {
        fputcsv($output, $row, ';');
    }
    fclose($output);
}

if (isset($_GET['start_date'], $_GET['end_date'])) {
    $start_date = $_GET['start_date'];
    $end_date = $_GET['end_date'];
    $status = $_GET['status'] ?? '';
    $arbeitsbereich = $_GET['arbeitsbereich'] ?? '';
    exportToCSV($start_date, $end_date, $status, $arbeitsbereich);
}
?>
