<?php
include '../config/database.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];
    $typ = $_POST['typ'];
    $adresse = $_POST['adresse'];
    $plz = $_POST['plz'];
    $kontakttelefonnummer = $_POST['kontakttelefonnummer'];
    $kontaktperson = $_POST['kontaktperson'];
    $informationen = $_POST['informationen'];

    $pdo = pdo();
    $stmt = $pdo->prepare("INSERT INTO arbeitsbereiche (Name, TYP, Adresse, PLZ, Kontakttelefonnummer, Kontaktperson, Informationen) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt->execute([$name, $typ, $adresse, $plz, $kontakttelefonnummer, $kontaktperson, $informationen]);

    header("Location: arbeitsbereiche.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Neuen Arbeitsbereich anlegen</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #6B8E23;
            color: #333;
        }
        .header {
            background-color: #6B8E23; /* Orange */
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .form-group label {
            color: #6B8E23; /* Orange */
        }
    </style>
</head>
<body>
<?php include 'navbar.php'; ?>
<h1 class="header">Neuen Arbeitsbereich anlegen</h1>
<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <form class="col-md-6" action="" method="post">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="typ">Typ:</label>
                <select class="form-control" id="typ" name="typ" required>
                    <option value="Verkaufsfläche">Verkaufsfläche</option>
                    <option value="Lager">Lager</option>
                    <option value="Logistik">Logistik</option>
                    <option value="Warenanlieferung">Warenanlieferung</option>
                    <option value="Hospitality">Hospitality</option>
                </select>
            </div>
            <div class="form-group">
                <label for="adresse">Adresse:</label>
                <input type="text" class="form-control" id="adresse" name="adresse" required>
            </div>
            <div class="form-group">
                <label for="plz">PLZ:</label>
                <input type="text" class="form-control" id="plz" name="plz" required>
            </div>
            <div class="form-group">
                <label for="kontakttelefonnummer">Kontakttelefonnummer:</label>
                <input type="text" class="form-control" id="kontakttelefonnummer" name="kontakttelefonnummer" required>
            </div>
            <div class="form-group">
                <label for="kontaktperson">Kontaktperson:</label>
                <input type="text" class="form-control" id="kontaktperson" name="kontaktperson" required>
            </div>
            <div class="form-group">
                <label for="informationen">Informationen:</label>
                <textarea class="form-control" id="informationen" name="informationen"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Arbeitsbereich Anlegen</button>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
