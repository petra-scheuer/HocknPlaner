<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter zu Diensten zuordnen</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #6B8E23;
            color: #333;
            font-family: Arial, sans-serif;
        }
    </style>
</head>
<body>
<div class="container mt-5">
    <h2>Mitarbeiter zu Diensten zuordnen</h2>
    <form method="post" action="mitarbeiter_zuweisen_verarbeiten.php">
        <div class="form-group">
            <label for="dienst_id">Dienst auswählen:</label>
            <select id="dienst_id" name="dienst_id" class="form-control">
                <?php foreach ($dienste as $dienst): ?>
                    <option value="<?= htmlspecialchars($dienst['termin_id']) ?>"><?= htmlspecialchars($dienst['Name']) ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="mitarbeiter_ids">Mitarbeiter auswählen:</label>
            <br>
            <?php foreach ($mitarbeiter as $mitarbeiter): ?>
                <input type="checkbox" id="mitarbeiter_<?= $mitarbeiter['MitarbeiterID'] ?>" name="mitarbeiter_ids[]" value="<?= $mitarbeiter['MitarbeiterID'] ?>">
                <label for="mitarbeiter_<?= $mitarbeiter['MitarbeiterID'] ?>"><?= htmlspecialchars($mitarbeiter['Vorname'] . " " . $mitarbeiter['Nachname']) ?></label><br>
            <?php endforeach; ?>
        </div>
        <button type="submit" name="action" value="assign" class="btn btn-primary">Zuweisen</button>
        <button type="submit" name="action" value="remove" class="btn btn-danger">Entfernen</button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
