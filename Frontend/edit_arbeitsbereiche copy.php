<?php
include '../config/database.php';

function updateArbeitsbereich($id, $name, $typ, $adresse, $plz, $kontakttelefonnummer, $kontaktperson, $informationen) {
    $pdo = pdo(); // Stellt sicher, dass die Datenbankverbindung vorhanden ist.
    $sql = "UPDATE arbeitsbereiche SET Name = ?, TYP = ?, Adresse = ?, PLZ = ?, Kontakttelefonnummer = ?, Kontaktperson = ?, Informationen = ? WHERE ArbeitsbereichID = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$name, $typ, $adresse, $plz, $kontakttelefonnummer, $kontaktperson, $informationen, $id]);
}

function deleteArbeitsbereich($id) {
    $pdo = pdo();
    $sql = "DELETE FROM arbeitsbereiche WHERE ArbeitsbereichID = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$id]);
}

$pdo = pdo();
$id = $_GET['id'];
$stmt = $pdo->prepare("SELECT * FROM arbeitsbereiche WHERE ArbeitsbereichID = ?");
$stmt->execute([$id]);
$bereich = $stmt->fetch(PDO::FETCH_ASSOC);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($_POST['action'] == 'save') {
        updateArbeitsbereich($id, $_POST['name'], $_POST['typ'], $_POST['adresse'], $_POST['plz'], $_POST['kontakttelefonnummer'], $_POST['kontaktperson'], $_POST['informationen']);
        header("Location: arbeitsbereiche.php"); // Umleitung nach dem Speichern
        exit;
    } elseif ($_POST['action'] == 'delete') {
        deleteArbeitsbereich($id);
        header("Location: arbeitsbereiche.php"); // Umleitung nach dem Löschen
        exit;
    }
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arbeitsbereich Verwaltung</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body { background-color: #f4f4f4; color: #333; }
        .header { background-color: #007bff; color: #ffffff; padding: 10px 0; text-align: center; }
        .btn-primary, .btn-danger { background-color: #007bff; border-color: #007bff; }
        .btn-danger { background-color: #dc3545; border-color: #dc3545; }
    </style>
</head>
<body>

<?php include 'navbar.php'; ?>
<h1 class="header">Arbeitsbereich bearbeiten</h1>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form method="post">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= htmlspecialchars($bereich['Name']) ?>">
                </div>
                <div class="form-group">
                    <label for="typ">Typ:</label>
                    <input type="text" class="form-control" id="typ" name="typ" value="<?= htmlspecialchars($bereich['TYP']) ?>">
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse:</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" value="<?= htmlspecialchars($bereich['Adresse']) ?>">
                </div>
                <div class="form-group">
                    <label for="plz">PLZ:</label>
                    <input type="text" class="form-control" id="plz" name="plz" value="<?= htmlspecialchars($bereich['PLZ']) ?>">
                </div>
                <div class="form-group">
                    <label for="kontakttelefonnummer">Kontakttelefonnummer:</label>
                    <input type="text" class="form-control" id="kontakttelefonnummer" name="kontakttelefonnummer" value="<?= htmlspecialchars($bereich['Kontakttelefonnummer']) ?>">
                </div>
                <div class="form-group">
                    <label for="kontaktperson">Kontaktperson:</label>
                    <input type="text" class="form-control" id="kontaktperson" name="kontaktperson" value="<?= htmlspecialchars($bereich['Kontaktperson']) ?>">
                </div>
                <div class="form-group">
                    <label for="informationen">Informationen:</label>
                    <textarea class="form-control" id="informationen" name="informationen"><?= htmlspecialchars($bereich['Informationen']) ?></textarea>
                </div>
                <button type="submit" name="action" value="save" class="btn btn-primary">Speichern</button>
                <button type="submit" name="action" value="delete" class="btn btn-danger" onclick="return confirm('Sind Sie sicher, dass Sie diesen Arbeitsbereich löschen möchten?');">Arbeitsbereich Löschen</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
