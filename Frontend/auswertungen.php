<?php include 'navbar.php';?>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Auswertungen Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .dashboard-button {
            width: 100%;
            margin-bottom: 20px; /* Abstand zwischen den Buttons */
        }
        
        body { background-color: #f4f4f4; color: #333; }
        .header { background-color: #6B8E23; color: #ffffff; padding: 10px 0; text-align: center; }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table { background-color: #ffffff; margin-top: 20px; }
        th { background-color: #6B8E23; color: #ffffff; }
    </style>
    </style>
</head>
<body>
    <h2 class="header">Auswertungen Dashboard</h2>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-4">
                <button onclick="location.href='keinDienst.php'" class="btn btn-primary dashboard-button">Mitarbeiter ohne Einsatz</button>
                <button onclick="location.href='stundenjeArbeitsbereich.php'" class="btn btn-primary dashboard-button">Monatliche Arbeitsstunden</button>
                <button onclick="location.href='.php'" class="btn btn-primary dashboard-button">weitere Auswertungen folgen</button>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
