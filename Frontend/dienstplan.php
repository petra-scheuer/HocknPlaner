<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include 'navbar.php';


include '../config/database.php';

function getAlleTermine($start_date, $end_date, $status = '', $typ = '', $arbeitsbereich = '') {
    $pdo = pdo();
    $sql = "
    SELECT t.termin_id AS id, t.*, a.Name AS ArbeitsbereichName, a.TYP AS ArbeitsbereichTyp,
    (SELECT COUNT(dm.mitarbeiter_id) FROM dienst_mitarbeiter dm WHERE dm.dienst_id = t.termin_id) AS AnzahlEingeteilteMitarbeiter
    FROM termine t
    LEFT JOIN arbeitsbereiche a ON t.arbeitsbereich_id = a.ArbeitsbereichID
    WHERE t.datum BETWEEN :start_date AND :end_date
    ";

    $params = ['start_date' => $start_date, 'end_date' => $end_date];

    if (!empty($status)) {
        $sql .= " AND t.status = :status";
        $params['status'] = $status;
    }

    if (!empty($typ)) {
        $sql .= " AND a.TYP = :typ";
        $params['typ'] = $typ;
    }

    if (!empty($arbeitsbereich)) {
        $sql .= " AND a.Name = :arbeitsbereich";
        $params['arbeitsbereich'] = $arbeitsbereich;
    }

    $sql .= " ORDER BY t.datum";
    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchAll();
}

// Funktion zum Abrufen eindeutiger Typen
function getUniqueTypes() {
    $pdo = pdo();
    $sql = "SELECT DISTINCT TYP FROM arbeitsbereiche";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_COLUMN);
}

// Funktion zum Abrufen eindeutiger Arbeitsbereiche
function getUniqueArbeitsbereiche() {
    $pdo = pdo();
    $sql = "SELECT DISTINCT Name FROM arbeitsbereiche";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_COLUMN);
}

if (isset($_GET['start_date']) && isset($_GET['end_date'])) {
    $start_date = $_GET['start_date'];
    $end_date = $_GET['end_date'];
} else {
    // Set default to current month
    $start_date = date('Y-m-01');
    $end_date = date('Y-m-t');
}
$status = $_GET['status'] ?? '';
$typ = isset($_GET['typ']) && $_GET['typ'] != '' ? $_GET['typ'] : ''; // Überprüfen, ob Typ gesetzt und nicht leer ist
$arbeitsbereich = isset($_GET['arbeitsbereich']) && $_GET['arbeitsbereich'] != '' ? $_GET['arbeitsbereich'] : ''; // Überprüfen, ob Arbeitsbereich gesetzt und nicht leer ist

$termine = getAlleTermine($start_date, $end_date, $status, $typ, $arbeitsbereich);
$uniqueTypes = getUniqueTypes();
$uniqueArbeitsbereiche = getUniqueArbeitsbereiche();

function berechnePause($dauer) {
    if ($dauer <= 6) {
        return 0;
    } elseif ($dauer > 6 && $dauer <= 8) {
        return 0.5;
    } else {
        return 1;
    }
}

?>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Dienstplan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
        }
        .header {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
        .new-service-btn {
            margin-top: 10px;
        }
    </style>
</head>
<body>

<h2 class="header">Dienstplan</h2>
<div class="container">

    <a href="neuer_dienst.php" class="btn btn-primary new-service-btn">Neuer Dienst</a>
    <form action="export.php" method="get" style="display: inline-block;">
    <input type="hidden" name="start_date" value="<?= htmlspecialchars($start_date) ?>">
    <input type="hidden" name="end_date" value="<?= htmlspecialchars($end_date) ?>">
    <input type="hidden" name="status" value="<?= htmlspecialchars($status) ?>">
    <input type="hidden" name="arbeitsbereich" value="<?= htmlspecialchars($arbeitsbereich) ?>">
    <button type="submit" class="btn btn-success">Exportieren</button>
</form>


<form class="filter-form" method="get" name="filter-form">
    <input type="date" class="form-control filter-input" name="start_date" value="<?= htmlspecialchars($start_date) ?>">
    <input type="date" class="form-control filter-input" name="end_date" value="<?= htmlspecialchars($end_date) ?>">
    <select class="form-control filter-input" name="status">
        <option value="" <?= empty($status) ? 'selected' : '' ?>>Alle Status</option>
        <option value="FORECAST" <?= $status == 'FORECAST' ? 'selected' : '' ?>>FORECAST</option>
        <option value="OFFEN" <?= $status == 'OFFEN' ? 'selected' : '' ?>>OFFEN</option>
        <option value="BESETZT" <?= $status == 'BESETZT' ? 'selected' : '' ?>>BESETZT</option>
        <option value="FERTIG" <?= $status == 'FERTIG' ? 'selected' : '' ?>>FERTIG</option>
        <option value="NACHBEARBEITET" <?= $status == 'NACHBEARBEITET' ? 'selected' : '' ?>>NACHBEARBEITET</option>
    </select>

    <select class="form-control filter-input" name="typ">
    <option value="">Alle Typen</option>
    <?php foreach ($uniqueTypes as $type): ?>
        <?php if ($type !== null): ?>
            <option value="<?= htmlspecialchars($type) ?>" <?= $type == $typ ? 'selected' : '' ?>>
                <?= htmlspecialchars($type) ?>
            </option>
        <?php endif; ?>
    <?php endforeach; ?>
</select>


<select class="form-control filter-input" name="arbeitsbereich">
    <option value="">Alle Arbeitsbereiche</option>
    <?php foreach ($uniqueArbeitsbereiche as $arbeitsbereichItem): ?>
        <option value="<?= htmlspecialchars($arbeitsbereichItem) ?>" <?= $arbeitsbereichItem == $arbeitsbereich ? 'selected' : '' ?>>
            <?= htmlspecialchars($arbeitsbereichItem) ?>
        </option>
    <?php endforeach; ?>
</select>

    <button type="submit" class="btn btn-primary">Filtern</button>
</form>

    <table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Arbeitsbereich</th>
            <th>Typ</th>
            <th>Datum</th>
            <th>Wochentag</th>
            <th>Beginn</th>
            <th>Ende</th>
            <th>Status</th>
            <th>Anzahl benötigter Mitarbeiter</th>
            <th>Anzahl eingeteilter Mitarbeiter</th>
            <th>Pause (Stunden)</th>
            <th>Dauer (Stunden)</th>
            <th>Details</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($termine as $termin): ?>
<tr>
    <td><?= htmlspecialchars($termin['id'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['ArbeitsbereichName'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['ArbeitsbereichTyp'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['datum'] ?? 'N/A') ?></td>
    <td>
        <?php
        if (!empty($termin['datum'])) {
            $date = new DateTime($termin['datum']);
            echo $date->format('D');
        } else {
            echo 'N/A';
        }
        ?>
    </td>
    <td><?= htmlspecialchars($termin['beginn'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['ende'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['status'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['anzahl'] ?? 'N/A') ?></td>
    <td><?= htmlspecialchars($termin['AnzahlEingeteilteMitarbeiter'] ?? 'N/A') ?></td>
    <?php
        if (!empty($termin['beginn']) && !empty($termin['ende'])) {
            $beginn = strtotime($termin['beginn']);
            $ende = strtotime($termin['ende']);
            $dauer = ($ende - $beginn) / 3600;
            $pause = berechnePause($dauer);
            $dauer -= $pause;
            $pauseText = number_format($pause, 2) . ' Std';
            $dauerText = number_format($dauer, 2) . ' Std';
        } else {
            $pauseText = 'N/A';
            $dauerText = 'N/A';
        }
    ?>
    <td><?= $pauseText ?></td>
    <td><?= $dauerText ?></td>
    <td>
        <a href="dienst_details.php?id=<?= urlencode($termin['id']) ?>" class="btn btn-info">Details</a>
    </td>
</tr>
<?php endforeach; ?>
    </tbody>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
