<?php
include 'navbar.php';
ini_set('display_errors', 1);
error_reporting(E_ALL);
// Weitere PHP-Code hier...
?>

<!-- HTML Inhalt hier, falls nötig -->


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HocknPlaner</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
        }
        .container {
            margin-top: 20px;
        }
        .left-half {
            float: left;
            width: 50%;
        }
        .right-half {
            float: right;
            width: 50%;
        }
        h1, h2 {
            color: #6B8E23; /* Orange */
        }
        p {
            font-size: 1.1rem;
        }
    </style>
</head>
<body>

    <div class="container mt-4">
        <div class="left-half">
            <h1>Willkommen beim HocknPlaner!</h1>
            <p>Hier sind deine aktuellen News:</p>
        </div>
        <div class="right-half">
            <!-- Offene Dienste für morgen -->
            <?php
            include '../config/database.php';

            // Funktion, um offene Dienste für morgen zu holen
            function getOffeneDiensteMorgen() {
                $pdo = pdo();
                $stmt = $pdo->prepare("
                    SELECT t.*, a.Name AS ArbeitsbereichName
                    FROM termine t
                    LEFT JOIN arbeitsbereiche a ON t.arbeitsbereich_id = a.ArbeitsbereichID
                    WHERE t.datum = DATE_ADD(CURDATE(), INTERVAL 1 DAY)
                    AND t.status = 'Offen'
                ");
                $stmt->execute();
                return $stmt->fetchAll();
            }

            $offene_dienste_morgen = getOffeneDiensteMorgen();
            ?>
            <h4>Offene Dienste für morgen:</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Arbeitsbereich</th>
                        <th>Datum</th>
                        <th>Beginn</th>
                        <th>Ende</th>
                        <th>Status</th>
                        <th>Anzahl benötigter Mitarbeiter</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($offene_dienste_morgen as $dienst): ?>
                        <tr>
                            <td><?= htmlspecialchars($dienst['ArbeitsbereichName']) ?></td>
                            <td><?= htmlspecialchars($dienst['datum']) ?></td>
                            <td><?= htmlspecialchars($dienst['beginn']) ?></td>
                            <td><?= htmlspecialchars($dienst['ende']) ?></td>
                            <td><?= htmlspecialchars($dienst['status']) ?></td>
                            <td><?= htmlspecialchars($dienst['anzahl']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <!-- Mitarbeiter ohne Einsatz im laufenden Monat -->
            

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
