<?php
include '../config/database.php';

// Check if form has been submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve dienst_id and mitarbeiter_id from POST data
    $dienst_id = isset($_POST['dienst_id']) ? intval($_POST['dienst_id']) : 0;
    $mitarbeiter_id = isset($_POST['mitarbeiter_id']) ? intval($_POST['mitarbeiter_id']) : 0;

    if ($dienst_id > 0 && $mitarbeiter_id > 0) {
        // Establish database connection
        $pdo = pdo();

        // Prepare SQL to insert the new assignment
        $sql = "INSERT INTO dienst_mitarbeiter (dienst_id, mitarbeiter_id) VALUES (:dienst_id, :mitarbeiter_id)";

        // Prepare statement
        $stmt = $pdo->prepare($sql);

        // Execute statement with parameters
        if ($stmt->execute(['dienst_id' => $dienst_id, 'mitarbeiter_id' => $mitarbeiter_id])) {
            echo "Mitarbeiter erfolgreich dem Dienst zugeordnet.";
        } else {
            echo "Fehler beim Zuordnen des Mitarbeiters zum Dienst.";
        }
    } else {
        echo "Ungültige Dienst- oder Mitarbeiter-ID.";
    }
}
?>

<!-- HTML form for assigning an employee to a service -->
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter zu Dienst zuordnen</title>
</head>
<body>
    <h2>Mitarbeiter zu einem Dienst zuordnen</h2>
    <form method="post">
        <label for="dienst_id">Dienst ID:</label>
        <input type="number" id="dienst_id" name="dienst_id" required><br><br>

        <label for="mitarbeiter_id">Mitarbeiter ID:</label>
        <input type="number" id="mitarbeiter_id" name="mitarbeiter_id" required><br><br>

        <input type="submit" value="Zuordnen">
    </form>
</body>
</html>
