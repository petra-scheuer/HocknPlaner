<?php
// Datenbankkonfiguration und Verbindung
include_once '../config/database.php';

// Übernahme der Daten aus dem POST-Request
$arbeitsbereich = $_POST['arbeitsbereich'] ?? '';
$datum = $_POST['datum'] ?? '';
$beginn = $_POST['beginn'] ?? '';
$ende = $_POST['ende'] ?? '';
$anzahl = $_POST['anzahl'] ?? '';

// Verbindung zur Datenbank herstellen
$pdo = pdo();

try {
    // Vorbereiten des SQL-Statements zur Einfügung der Daten
    $stmt = $pdo->prepare("INSERT INTO termine (arbeitsbereich, datum, beginn, ende, anzahl) VALUES (:arbeitsbereich, :datum, :beginn, :ende, :anzahl)");

    // Binden der Parameter an das SQL-Statement
    $stmt->bindParam(':arbeitsbereich', $arbeitsbereich);
    $stmt->bindParam(':datum', $datum);
    $stmt->bindParam(':beginn', $beginn);
    $stmt->bindParam(':ende', $ende);
    $stmt->bindParam(':anzahl', $anzahl);

    // Ausführen des SQL-Statements
    $stmt->execute();

    // Erfolgsmeldung
    echo "Der Termin wurde erfolgreich gespeichert!";
} catch (PDOException $e) {
    // Fehlermeldung bei einer Ausnahme
    die("Fehler beim Speichern des Termins: " . $e->getMessage());
}
?>
