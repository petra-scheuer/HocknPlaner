<?php
// Datenbankkonfiguration einbinden
include '../config/database.php';
include 'navbar.php';

$pdo = pdo(); // Hier wird die Funktion aufgerufen, um die PDO-Instanz zu



// PDO-Verbindung vorausgesetzt
try {
    // SQL-Abfrage vorbereiten
    $sql = "SELECT ab.Name AS Arbeitsbereich, SUM(TIMESTAMPDIFF(HOUR, t.beginn, t.ende)) AS Gesamtstunden
            FROM termine t
            INNER JOIN arbeitsbereiche ab ON t.arbeitsbereich_id = ab.ArbeitsbereichID
            GROUP BY ab.Name";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    // Ergebnisse in ein Array speichern
    $arbeitsstundenProBereich = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die("Datenbankfehler: " . $e->getMessage());
} catch (Exception $e) {
    die("Allgemeiner Fehler: " . $e->getMessage());
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Auswertungen: Arbeitsstunden pro Arbeitsbereich</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f4f4f4;
            color: #333;
        }
        .header {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table {
            background-color: #ffffff;
            margin-top: 20px;
        }
        th {
            background-color: #6B8E23;
            color: #ffffff;
        }
        .filter-form {
            margin: 20px 0;
            display: flex;
            justify-content: center;
        }
        .filter-input {
            margin-right: 10px;
            flex-grow: 1;
        }
        .filter-legend {
            margin-top: 10px;
            display: flex;
            justify-content: center;
        }
        .filter-link {
            margin: 0 5px;
        }
    </style>
</head>
<body>
    <!-- Navigation einfügen -->

    <h2 class="header">Arbeitsstunden pro Arbeitsbereich</h2>

    <!-- Hauptinhalt -->
    <div class="container">

        <!-- Statistik-Tabelle -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Arbeitsbereich</th>
                    <th>Gesamtstunden</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($arbeitsstundenProBereich as $row): ?>
                    <tr>
                        <td><?= isset($row['Arbeitsbereich']) ? htmlspecialchars($row['Arbeitsbereich']) : 'N/A' ?></td>
                        <td><?= isset($row['Gesamtstunden']) ? htmlspecialchars($row['Gesamtstunden']) : 'N/A' ?> Stunden</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <!-- JavaScript-Bibliotheken einfügen -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
