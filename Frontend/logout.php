<?php
session_start();

// Die aktuelle Sitzung zerstören
session_unset();
session_destroy();

// Weiterleitung zur Login-Seite
header('Location: index.php');
exit;
?>
