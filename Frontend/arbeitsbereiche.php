<?php
include '../config/database.php';
include 'navbar.php';


function fetchArbeitsbereiche() {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT * FROM arbeitsbereiche ORDER BY Name ASC");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$arbeitsbereiche = fetchArbeitsbereiche();

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arbeitsbereiche</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body { color: #333; }
        .header { background-color: #6B8E23; color: #ffffff; padding: 10px 0; text-align: center; }
        .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
            background-color: #6B8E23 !important;
            border-color: #6B8E23 !important;
        }
        .table { background-color: #ffffff; margin-top: 20px; }
        th { background-color: #6B8E23; color: #ffffff; }
    </style>
</head>
<body>


<h2 class="header">Arbeitsbereiche</h2>

<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Typ</th>
                <th>Adresse</th>
                <th>PLZ</th>
                <th>Kontakttelefonnummer</th>
                <th>Kontaktperson</th>
                <th>Informationen</th>
                <th>Aktionen</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($arbeitsbereiche as $bereich) : ?>
                <tr>
                    <td><?= htmlspecialchars($bereich['Name']) ?></td>
                    <td><?= htmlspecialchars($bereich['TYP']) ?></td>
                    <td><?= htmlspecialchars($bereich['Adresse']) ?></td>
                    <td><?= htmlspecialchars($bereich['PLZ']) ?></td>
                    <td><?= htmlspecialchars($bereich['Kontakttelefonnummer']) ?></td>
                    <td><?= htmlspecialchars($bereich['Kontaktperson']) ?></td>
                    <td><?= htmlspecialchars($bereich['Informationen']) ?></td>
                    <td>
                        <a href="arbeitsbereiche_details.php?id=<?= $bereich['ArbeitsbereichID'] ?>" class="btn btn-primary">Details</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
