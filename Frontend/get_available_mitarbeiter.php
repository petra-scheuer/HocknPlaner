<?php
include '../config/database.php';

if (isset($_GET['arbeitsbereich_id'])) {
    $arbeitsbereich_id = $_GET['arbeitsbereich_id'];
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT m.MitarbeiterID, m.Vorname, m.Nachname FROM mitarbeiter m
                           WHERE m.Status = 'AKTIV' AND m.MitarbeiterID NOT IN (
                               SELECT ma.mitarbeiter_id FROM mitarbeiter_arbeitsbereich ma WHERE ma.arbeitsbereich_id = ?)");
    $stmt->execute([$arbeitsbereich_id]);
    $mitarbeiter = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($mitarbeiter);
    exit;
}
?>
