<?php
include '../config/database.php'; // Pfad ggf. anpassen

$pdo = pdo(); // PDO-Verbindung herstellen

function getActiveMitarbeiter($pdo) {
    $sql = "SELECT MitarbeiterID, Vorname, Nachname FROM mitarbeiter WHERE Status = 'aktiv' ORDER BY Nachname, Vorname";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getCountEinsaetze($pdo, $monatJahr) {
    $startDatum = $monatJahr . '-01';
    $endDatum = date('Y-m-t', strtotime($startDatum));
    $sql = "SELECT dm.mitarbeiter_id, COUNT(*) as einsatz_count FROM dienst_mitarbeiter dm
            INNER JOIN termine t ON dm.dienst_id = t.termin_id
            WHERE t.datum BETWEEN :startDatum AND :endDatum
            GROUP BY dm.mitarbeiter_id";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':startDatum', $startDatum);
    $stmt->bindParam(':endDatum', $endDatum);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$einsaetze = [];
$aktuellesMonatJahr = date('Y-m'); // Default auf aktuelles Jahr und Monat setzen
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['monat'])) {
    $aktuellesMonatJahr = $_POST['monat'];
}
$einsaetze = getCountEinsaetze($pdo, $aktuellesMonatJahr);
$mitarbeiter = getActiveMitarbeiter($pdo);
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter Einsatzübersicht</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body { background-color: #f4f4f4; }
        h1 { color: #6B8E23; margin-top: 20px; }
    </style>
</head>
<body>
<?php include 'navbar.php'; ?>

<div class="container">
    <h1>Einsatzübersicht der Mitarbeiter für <?php echo $aktuellesMonatJahr; ?></h1>
    <form action="" method="post" class="mb-3">
        <div class="form-group">
            <label for="monat">Monat wählen:</label>
            <input type="month" id="monat" name="monat" value="<?php echo $aktuellesMonatJahr; ?>" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Einsatzübersicht anzeigen</button>
    </form>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Mitarbeiter</th>
                    <th>Anzahl der Einsätze</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $einsatzDaten = [];
                foreach ($mitarbeiter as $person) {
                    $found = false;
                    foreach ($einsaetze as $einsatz) {
                        if ($einsatz['mitarbeiter_id'] == $person['MitarbeiterID']) {
                            $einsatzDaten[$person['MitarbeiterID']] = $einsatz['einsatz_count'];
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $einsatzDaten[$person['MitarbeiterID']] = 0;
                    }
                }
                asort($einsatzDaten); // Sortiert die Mitarbeiter, beginnend mit denjenigen, die die wenigsten Einsätze haben

                foreach ($einsatzDaten as $id => $count) {
                    foreach ($mitarbeiter as $person) {
                        if ($person['MitarbeiterID'] == $id) {
                            echo "<tr><td>" . htmlspecialchars($person['Nachname'] . ', ' . $person['Vorname']) . "</td><td>" . $count . "</td></tr>";
                            break;
                        }
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
