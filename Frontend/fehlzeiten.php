<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include '../config/database.php';
include 'navbar.php';

// Funktion, um eine neue Fehlzeit in die Datenbank einzutragen
function addFehlzeit($mitarbeiter_id, $typ, $beginn, $ende, $beschreibung) {
    $pdo = pdo();
    $sql = "INSERT INTO fehlzeiten (mitarbeiter_id, typ, beginn, ende, beschreibung) VALUES (?, ?, ?, ?, ?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$mitarbeiter_id, $typ, $beginn, $ende, $beschreibung]);
}

// Funktion, um alle Fehlzeiten basierend auf Filtern zu holen
function getFilteredFehlzeiten($typFilter, $monatFilter) {
    $pdo = pdo();
    $year = date('Y');
    $firstDayOfMonth = date("Y-m-d", strtotime("$year-$monatFilter-01"));
    $lastDayOfMonth = date("Y-m-t", strtotime("$year-$monatFilter-01"));
    $query = "SELECT f.*, m.Vorname, m.Nachname FROM fehlzeiten f JOIN mitarbeiter m ON f.mitarbeiter_id = m.MitarbeiterID WHERE beginn BETWEEN ? AND ?";
    $params = [$firstDayOfMonth, $lastDayOfMonth];

    if ($typFilter != 'Alle') {
        $query .= " AND typ = ?";
        $params[] = $typFilter;
    }

    $stmt = $pdo->prepare($query);
    $stmt->execute($params);
    return $stmt->fetchAll();
}

// Funktion, um alle Mitarbeiter zu holen, sortiert nach Nachname
function getAllMitarbeiter() {
    $pdo = pdo();
    $stmt = $pdo->prepare("SELECT MitarbeiterID, Personalnummer, Vorname, Nachname FROM mitarbeiter ORDER BY Nachname, Vorname");
    $stmt->execute();
    return $stmt->fetchAll();
}

$mitarbeiter = getAllMitarbeiter();
$typFilter = $_POST['typFilter'] ?? 'Alle';
$monatFilter = $_POST['monatFilter'] ?? date('m');
$fehlzeiten = getFilteredFehlzeiten($typFilter, $monatFilter);

// Verarbeitung des Formulars für neue Fehlzeiten
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['addFehlzeit'])) {
    addFehlzeit($_POST['mitarbeiter_id'], $_POST['typ'], $_POST['beginn'], $_POST['ende'], $_POST['beschreibung'] ?? '');
    echo "<p>Fehlzeit erfolgreich hinzugefügt!</p>";
    $fehlzeiten = getFilteredFehlzeiten($typFilter, $monatFilter); // Aktualisiere die Liste nach Hinzufügung
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Fehlzeiten Verwaltung</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        body {
            background-color: ;
            color: #333;
        }
        .header, .table th {
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .table {
            margin-top: 20px;
        }
        .form-control, .btn {
            margin-top: 10px;
        }
        
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="header">Aktuelle Fehlzeiten</h2>
            <form method="post">
                <div class="form-group">
                    <label for="typFilter">Filter nach Art:</label>
                    <select id="typFilter" name="typFilter" class="form-control">
                        <option>Alle</option>
                        <option value="Urlaub" <?= $typFilter == 'Urlaub' ? 'selected' : '' ?>>Urlaub</option>
                        <option value="Krankenstand" <?= $typFilter == 'Krankenstand' ? 'selected' : '' ?>>Krankenstand</option>
                        <option value="Dienstabsage" <?= $typFilter == 'Dienstabsage' ? 'selected' : '' ?>>Dienstabsage</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="monatFilter">Filter nach Monat:</label>
                    <select id="monatFilter" name="monatFilter" class="form-control">
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>" <?= $monatFilter == $i ? 'selected' : '' ?>><?= date('F', mktime(0, 0, 0, $i, 10)) ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Filtern</button>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Mitarbeiter</th>
                        <th>Typ</th>
                        <th>Beginn</th>
                        <th>Ende</th>
                        <th>Beschreibung</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fehlzeiten as $f): ?>
                        <tr>
                            <td><?= htmlspecialchars($f['Nachname'] . ' ' . $f['Vorname']) ?></td>
                            <td><?= htmlspecialchars($f['typ']) ?></td>
                            <td><?= htmlspecialchars($f['beginn']) ?></td>
                            <td><?= htmlspecialchars($f['ende']) ?></td>
                            <td><?= htmlspecialchars($f['beschreibung']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <h2 class="header">Fehlzeiten erfassen</h2>
            <form method="post">
                <input type="hidden" name="addFehlzeit" value="1">
                <div class="form-group">
                    <label for="mitarbeiter_id">Mitarbeiter auswählen:</label>
                    <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                        <?php foreach ($mitarbeiter as $m): ?>
                            <option value="<?= htmlspecialchars($m['MitarbeiterID']) ?>"><?= htmlspecialchars($m['Personalnummer'] . '-' . $m['Nachname'] . ' ' . $m['Vorname']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="typ">Art der Fehlzeit</label>
                    <select class="form-control" name="typ" required>
                        <option>Urlaub</option>
                        <option>Krankenstand</option>
                        <option>Dienstabsage</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="beginn">Beginn</label>
                    <input type="date" class="form-control" name="beginn" required>
                </div>
                <div class="form-group">
                    <label for="ende">Ende</label>
                    <input type="date" class="form-control" name="ende" required>
                </div>
                <div class="form-group">
                    <label for="beschreibung">Beschreibung (optional)</label>
                    <textarea class="form-control" name="beschreibung"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Eintragen</button>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
