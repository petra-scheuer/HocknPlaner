<?php
session_start();
include '../config/database.php';
include 'navbar.php';

// Überprüfen, ob der Benutzer eingeloggt ist
if (!isset($_SESSION['loggedin'])) {
    echo "Benutzer ist nicht eingeloggt.";
} else {
    echo "Benutzer ist eingeloggt als: " . $_SESSION['username'];

    // Verbindung zur Datenbank herstellen
    $pdo = pdo();

    // Benutzerdaten aus der Datenbank abrufen
    $stmt = $pdo->prepare("SELECT role FROM users WHERE username = ?");
    $stmt->execute([$_SESSION['username']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    // Benutzerrolle in die Session speichern
    $_SESSION['role'] = $user['role'];

    // Überprüfen, ob der Benutzer als Administrator eingeloggt ist
    if ($_SESSION['role'] === 'admin') {
        echo "Der Benutzer ist ein Administrator.";

        // Wenn das Formular gesendet wurde und Benutzer ein Administrator ist
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Formulardaten abrufen
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);
            $role = trim($_POST['role']);

            // Überprüfen, ob Benutzername und Passwort nicht leer sind
            if (empty($username) || empty($password)) {
                $error = "Benutzername und Passwort dürfen nicht leer sein.";
            } else {
                // Überprüfen, ob der Benutzername bereits existiert
                $stmt = $pdo->prepare("SELECT id FROM users WHERE username = ?");
                $stmt->execute([$username]);
                if ($stmt->rowCount() > 0) {
                    $error = "Dieser Benutzername ist bereits vergeben.";
                } else {
                    // Passwort hashen
                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);

                    // Neuen Benutzer in die Datenbank einfügen
                    $stmt = $pdo->prepare("INSERT INTO users (username, password_hash, role) VALUES (?, ?, ?)");
                    if ($stmt->execute([$username, $passwordHash, $role])) {
                        $error = "Benutzer erfolgreich angelegt.";
                    } else {
                        $error = "Es gab einen Fehler beim Anlegen des Benutzers.";
                    }
                }
            }
        }
    } else {
        echo "Der Benutzer ist kein Administrator.";

        // Wenn das Formular gesendet wurde und Benutzer kein Administrator ist
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Formulardaten abrufen
            $subject = trim($_POST['subject']);
            $message = trim($_POST['message']);

            // Einfügen des Tickets in die Datenbank
            $stmt = $pdo->prepare("INSERT INTO tickets (subject, message, user_id) VALUES (?, ?, ?)");
            if ($stmt->execute([$subject, $message, $_SESSION['user_id']])) {
                $error = "Ticket erfolgreich erstellt.";
            } else {
                $error = "Es gab einen Fehler beim Erstellen des Tickets.";
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Admin Bereich</title>
</head>
<body>

    <h1>Admin-Bereich: Benutzer anlegen</h1>
    
    <!-- Nur Administratoren können Benutzer anlegen -->
    <?php if ($_SESSION['role'] === 'admin'): ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div>
                <label>Benutzername:</label>
                <input type="text" name="username" required>
            </div>
            <div>
                <label>Passwort:</label>
                <input type="password" name="password" required>
            </div>
            <div>
                <label>Rolle:</label>
                <select name="role">
                    <option value="admin">Administrator</option>
                    <option value="user">Benutzer</option>
                </select>
            </div>
            <div>
                <input type="submit" value="Benutzer anlegen">
            </div>
        </form>
    <?php endif; ?>

    <!-- Alle Benutzer können Tickets erstellen -->
    <h1>Ticket erstellen</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div>
            <label>Betreff:</label>
            <input type="text" name="subject" required>
        </div>
        <div>
            <label>Nachricht:</label>
            <textarea name="message" required></textarea>
        </div>
        <div>
            <input type="submit" value="Ticket erstellen">
        </div>
    </form>

</body>
</html>
