<?php
ob_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../config/database.php';
include 'navbar.php';


// Dienst ID aus GET-Parameter lesen
$termin_id = isset($_GET['id']) ? intval($_GET['id']) : 0;

// Verbindung zur Datenbank über PDO herstellen
$pdo = pdo();

// SQL-Abfrage vorbereiten für die Dienstdetails
$sql = "SELECT t.*, a.* FROM termine t JOIN arbeitsbereiche a ON t.arbeitsbereich_id = a.ArbeitsbereichID WHERE t.termin_id = :termin_id";
$stmt = $pdo->prepare($sql);
$stmt->execute(['termin_id' => $termin_id]);

$dienst = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$dienst) {
    header("Location: dienstplan.php");
    exit;
}

// Pausenzeit und benötigte Mitarbeiter
$start = new DateTime($dienst['beginn']);
$ende = new DateTime($dienst['ende']);
$dauer = $start->diff($ende)->h + ($start->diff($ende)->i / 60);
$pause = ($dauer > 6 && $dauer <= 8) ? 0.5 : ($dauer > 8 ? 1 : 0);
$benoetigte_mitarbeiter = $dienst['anzahl'];

// Abrufen der Mitarbeiter, die bereits dem Dienst zugeordnet sind
$zugeordnet_sql = "SELECT m.mitarbeiterID, m.Personalnummer, m.Vorname, m.Nachname FROM mitarbeiter m JOIN dienst_mitarbeiter dm ON m.mitarbeiterID = dm.mitarbeiter_id WHERE dm.dienst_id = :dienst_id";
$zugeordnet_stmt = $pdo->prepare($zugeordnet_sql);
$zugeordnet_stmt->execute(['dienst_id' => $termin_id]);
$zugeordnete_mitarbeiter = $zugeordnet_stmt->fetchAll(PDO::FETCH_ASSOC);

// Abrufen der Mitarbeiter, die dem Arbeitsbereich zugeordnet sind
$arbeitsbereich_mitarbeiter_sql = "SELECT m.mitarbeiterID, m.Personalnummer, m.Vorname, m.Nachname FROM mitarbeiter m JOIN mitarbeiter_arbeitsbereich ma ON m.mitarbeiterID = ma.mitarbeiter_id WHERE ma.arbeitsbereich_id = :arbeitsbereich_id";
$arbeitsbereich_mitarbeiter_stmt = $pdo->prepare($arbeitsbereich_mitarbeiter_sql);
$arbeitsbereich_mitarbeiter_stmt->execute(['arbeitsbereich_id' => $dienst['arbeitsbereich_id']]);
$arbeitsbereich_mitarbeiter = $arbeitsbereich_mitarbeiter_stmt->fetchAll(PDO::FETCH_ASSOC);

// Fehlermeldung initialisieren
$errorMessage = "";

// Code zum Überprüfen der Mitarbeiterzuweisung und Aktualisierung des Dienststatus
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Überprüfen, ob eine Mitarbeiter-ID übergeben wurde
    if (isset($_POST['mitarbeiter_id'])) {
        $mitarbeiter_id = $_POST['mitarbeiter_id'];

        // Überprüfen, ob der ausgewählte Mitarbeiter dem Arbeitsbereich zugeordnet ist
        $isValidAssignment = false;
        foreach ($arbeitsbereich_mitarbeiter as $arbeitsbereich_mitarbeiter) {
            if ($arbeitsbereich_mitarbeiter['mitarbeiterID'] == $mitarbeiter_id) {
                $isValidAssignment = true;
                break;
            }
        }

        if (!$isValidAssignment) {
            $errorMessage = "Der ausgewählte Mitarbeiter ist dem Arbeitsbereich nicht zugeordnet.";
        } else {
            // SQL-Statement vorbereiten, um den Mitarbeiter dem Dienst zuzuweisen
            $zuzuweisende_sql = "INSERT INTO dienst_mitarbeiter (dienst_id, mitarbeiter_id) VALUES (:dienst_id, :mitarbeiter_id)";
            $zuzuweisende_stmt = $pdo->prepare($zuzuweisende_sql);

            // Überprüfen, ob der Mitarbeiter bereits eingeteilt ist
            $existingAssignmentCheckSql = "SELECT COUNT(*) FROM dienst_mitarbeiter WHERE dienst_id = :dienst_id AND mitarbeiter_id = :mitarbeiter_id";
            $existingAssignmentCheckStmt = $pdo->prepare($existingAssignmentCheckSql);
            $existingAssignmentCheckStmt->execute(['dienst_id' => $termin_id, 'mitarbeiter_id' => $mitarbeiter_id]);
            $assignmentCount = $existingAssignmentCheckStmt->fetchColumn();

            if ($assignmentCount > 0) {
                $errorMessage = "Der Mitarbeiter ist bereits für diesen Dienst eingeteilt.";
            } else {
                // Parameter binden und das Statement ausführen
                $zuzuweisende_stmt->execute(['dienst_id' => $termin_id, 'mitarbeiter_id' => $mitarbeiter_id]);

                // Dienststatus aktualisieren
                $countAssignedEmployees = count($zugeordnete_mitarbeiter) + 1;
                if ($countAssignedEmployees >= $benoetigte_mitarbeiter) {
                    // Status auf "besetzt" ändern
                    $updateStatusSql = "UPDATE termine SET status = 'besetzt' WHERE termin_id = :termin_id";
                    $updateStatusStmt = $pdo->prepare($updateStatusSql);
                    $updateStatusStmt->execute(['termin_id' => $termin_id]);
                }

                // Weiterleitung zur Seite mit den Dienstdetails
                header("Location: dienst_details.php?id=" . $termin_id);
                exit;
            }
        }
    }
}

// Code zum Löschen von Mitarbeiterzuweisungen und Aktualisierung des Dienststatus
if (isset($_GET['remove']) && isset($_GET['mitarbeiter_id'])) {
    $removeMitarbeiterId = intval($_GET['mitarbeiter_id']);
    $removeAssignmentSql = "DELETE FROM dienst_mitarbeiter WHERE dienst_id = :dienst_id AND mitarbeiter_id = :mitarbeiter_id";
    $removeAssignmentStmt = $pdo->prepare($removeAssignmentSql);
    $removeAssignmentStmt->execute(['dienst_id' => $termin_id, 'mitarbeiter_id' => $removeMitarbeiterId]);

    // Dienststatus aktualisieren
    $countAssignedEmployees = count($zugeordnete_mitarbeiter) - 1;
    if ($countAssignedEmployees < $benoetigte_mitarbeiter) {
        // Status auf "offen" belassen
        $updateStatusSql = "UPDATE termine SET status = 'offen' WHERE termin_id = :termin_id";
        $updateStatusStmt = $pdo->prepare($updateStatusSql);
        $updateStatusStmt->execute(['termin_id' => $termin_id]);
    }

   	header("Location: dienst_details.php?id=" . $termin_id);
    exit;
  // Ende des Output Buffers nach allen Header-Änderungen
ob_end_flush();
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dienstdetails</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body { background-color: #f4f4f4; color: #333; font-family: Arial, sans-serif; }
        .header, .btn-primary, th { background-color: #6B8E23; color: #ffffff; }
        .card-custom { box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 10px; }
        .main-title { font-size: 30px; font-weight: bold; color: #6B8E23; }
        .detail-title { font-size: 18px; font-weight: bold; color: #7; }
        .error-message { color: red; }
        .dropdown-menu { max-height: 200px; overflow-y: auto; }
        .remove-btn { float: right; }
    </style>
</head>
<body>

<div class="container mt-5">
    <h2 class="main-title">Dienstdetails - <?= htmlspecialchars($dienst['Name']) ?></h2>
    <?php if (isset($errorMessage)): ?>
        <p class="error-message"><?= $errorMessage ?></p>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-custom p-3">
                <div class="detail-section">
                    <p class="detail-title">Dienstinformationen</p>
                    <table class="table">
                        <tr><td>Datum:</td><td><?= htmlspecialchars($dienst['datum']) ?></td></tr>
                        <tr><td>Startzeit:</td><td><?= htmlspecialchars($dienst['beginn']) ?></td></tr>
                        <tr><td>Endzeit:</td><td><?= htmlspecialchars($dienst['ende']) ?></td></tr>
                        <tr><td>Dauer:</td><td><?= $dauer ?> Stunden</td></tr>
                        <tr><td>Pause:</td><td><?= $pause ?> Stunden</td></tr>
                        <tr><td>Arbeitsbereich:</td><td><?= htmlspecialchars($dienst['Name']) ?></td></tr>
                        <tr><td>Typ:</td><td><?= htmlspecialchars($dienst['TYP']) ?></td></tr>
                        <tr><td>Adresse:</td><td><?= htmlspecialchars($dienst['Adresse']) ?></td></tr>
                        <tr><td>Benötigte Mitarbeiter:</td><td><?= $benoetigte_mitarbeiter ?></td></tr>
                        <tr><td>Eingeteilte Mitarbeiter:</td><td><?= count($zugeordnete_mitarbeiter) ?></td></tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-custom p-3">
                <div class="detail-section">
                    <p class="detail-title">Mitarbeiter zuweisen</p>
                    <form method="post">
                        <label for="mitarbeiter_id">Mitarbeiter auswählen:</label>
                        <select id="mitarbeiter_id" name="mitarbeiter_id" class="form-control">
                            <?php foreach ($arbeitsbereich_mitarbeiter as $mitarbeiter): ?>
                                <option value="<?= $mitarbeiter['mitarbeiterID'] ?>">
                                    <?= htmlspecialchars($mitarbeiter['mitarbeiterID']. '-' .$mitarbeiter['Nachname'] . ' ' . $mitarbeiter['Vorname']) ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <button type="submit" class="btn btn-primary mt-2">Zuweisen</button>
                    </form>
                </div>
            </div>
            <?php if ($zugeordnete_mitarbeiter): ?>
                <div class="card card-custom p-3 mt-3">
                    <div class="detail-section">
                        <p class="detail-title">Eingeteilte Mitarbeiter</p>
                        <table class="table table-bordered">
                            <?php foreach ($zugeordnete_mitarbeiter as $mitarbeiter): ?>
                                <tr>
                                    <td><?= $mitarbeiter['mitarbeiterID'] . '-' . $mitarbeiter['Personalnummer'] ?></td>
                                    <td><?= htmlspecialchars($mitarbeiter['Nachname'] . ' ' . $mitarbeiter['Vorname']) ?></td>
                                    <td>
                                        <a href="dienst_details.php?id=<?= $termin_id ?>&remove=true&mitarbeiter_id=<?= $mitarbeiter['mitarbeiterID'] ?>" class="btn btn-danger btn-sm remove-btn">Entfernen</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <a href="dienstplan.php" class="btn btn-primary mt-2">Zurück zur Übersicht</a>
        </div>
    </div>
    <?php if (!$dienst): ?>
        <p>Dienstdetails konnten nicht geladen werden.</p>
    <?php endif; ?>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
