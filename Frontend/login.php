<?php
session_start();
include '../config/database.php'; // Pfad ggf. anpassen
$username = ""; // Initialisiert die Variable mit einem leeren String


// Prüfen, ob der Benutzer bereits eingeloggt ist
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    header('Location: home.php');
    exit;
}

// Variablen für Fehlermeldungen initialisieren
$username_err = $password_err = $login_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $pdo = pdo();

    // Benutzername und Passwort validieren
    if (empty(trim($_POST["username"]))) {
        $username_err = "Bitte geben Sie den Benutzernamen ein.";
    } else {
        $username = trim($_POST["username"]);
    }

    if (empty(trim($_POST["password"]))) {
        $password_err = "Bitte geben Sie Ihr Passwort ein.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validieren der Anmeldedaten
    if (empty($username_err) && empty($password_err)) {
        $stmt = $pdo->prepare('SELECT id, username, password_hash FROM users WHERE username = :username');
        
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        
        if ($stmt->execute()) {
            // Prüfen, ob der Benutzername existiert, dann Passwort verifizieren
            if ($stmt->rowCount() == 1) {
                if ($row = $stmt->fetch()) {
                    $id = $row['id'];
                    $username = $row['username'];
                    $hashed_password = $row['password_hash'];
                    if (password_verify($password, $hashed_password)) {
                        // Passwort ist korrekt, starten einer neuen Session
                        $_SESSION['loggedin'] = true;
                        $_SESSION['id'] = $id;
                        $_SESSION['username'] = $username;
                        
                        // Weiterleitung zum Home-Bereich
                        header('Location: home.php');
                        exit;
                    } else {
                        // Passwort ist nicht korrekt
                        $login_err = "Ungültige Anmeldedaten.";
                    }
                }
            } else {
                $login_err = "Ungültige Anmeldedaten.";
            }
        } else {
            echo "Oops! Etwas ist schief gelaufen. Bitte versuchen Sie es später noch einmal.";
        }

        // Schließen der Anweisung
        unset($stmt);
    }

    // Schließen der Verbindung
    unset($pdo);
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Login - Hocknplaner</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .navbar-custom {
            background-color: #6B8E23; /* Dunkleres Grün für die Navbar */
        }
        .navbar-custom .navbar-nav .nav-link {
            color: white; /* Weiße Schriftfarbe für bessere Sichtbarkeit */
        }
        .navbar-custom .navbar-nav .nav-link:hover,
        .navbar-custom .navbar-nav .nav-link:focus {
            color: #A5D6A7; /* Aufgehelltes Grün für Hover- und Fokus-Effekte */
        }
        .navbar-custom .navbar-brand {
            color: white; /* Weiße Schriftfarbe für den Markennamen */
            display: flex;
            align-items: center;
        }
        .navbar-custom .navbar-brand img {
            height: 60px; /* Vergrößerte Höhe des Logos */
            margin-right: 10px; /* Abstand zwischen Logo und Text */
        }
        .navbar-custom .navbar-toggler {
            border-color: #A5D6A7; /* Angepasstes helleres Grün für den Umschaltknopf */
        }
        .navbar-custom .navbar-toggler-icon {
            background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3e%3cpath stroke='rgba(165, 214, 167, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e");
            /* Benutzerdefinierte SVG für das Toggler-Icon */
        }
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            padding: 20px;
        }
        .container {
            background: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        h2.header {
            background-color: #6B8E23;
            color: white;
            padding: 10px 0;
            text-align: center;
            border-radius: 8px 8px 0 0;
            margin-top: 0;
        }
        form {
            margin-top: 20px;
        }
        label {
            margin-bottom: 5px;
            display: block;
        }
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }
        input[type="submit"] {
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #45a049;
        }
        .form-group {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-custom">
        <a class="navbar-brand" href="index.php">
            <img src="../assets/logo.png" alt="Logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            </ul>
        </div>
    </nav>
    <div class="container">
        <h2 class="header">Login</h2>
        <p>Bitte füllen Sie die Felder aus, um sich anzumelden.</p>
        <?php 
        if(!empty($login_err)){
            echo '<div class="alert alert-danger">' . $login_err . '</div>';
        }        
        ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Benutzername</label>
                <input type="text" name="username" class="<?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Passwort</label>
                <input type="password" name="password" class="<?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" value="Login">
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
