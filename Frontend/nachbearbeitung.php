<?php
// database.php einbinden
include 'navbar.php';
include '../config/database.php'; // Pfad ggf. anpassen

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Funktion zum Aktualisieren der Dienstzeiten und des Status
function updateServiceTime($dienst_id, $neuer_beginn, $neues_ende)
{
    try {
        // Datenbankverbindung herstellen
        $db = pdo(); // Verwende die pdo() Funktion aus der Konfigurationsdatei

        // SQL-Abfrage vorbereiten, um Dienstzeit und Status zu aktualisieren
        $query = $db->prepare("UPDATE termine SET beginn = :neuer_beginn, ende = :neues_ende, status = 'NACHBEARBEITET' WHERE termin_id = :dienst_id");

        // SQL-Abfrage ausführen
        $query->execute(array(':neuer_beginn' => $neuer_beginn, ':neues_ende' => $neues_ende, ':dienst_id' => $dienst_id));

        // Erfolgsmeldung
        return "Dienstzeit erfolgreich aktualisiert.";
    } catch (PDOException $e) {
        // Fehlermeldung
        return "Fehler beim Aktualisieren der Dienstzeit: " . $e->getMessage();
    }
}

// Funktion zum Bestätigen der Dienstzeit und Aktualisieren des Status
function confirmServiceTime($dienst_id)
{
    try {
        // Datenbankverbindung herstellen
        $db = pdo(); // Verwende die pdo() Funktion aus der Konfigurationsdatei

        // SQL-Abfrage vorbereiten, um Status zu aktualisieren
        $query = $db->prepare("UPDATE termine SET status = 'NACHBEARBEITET' WHERE termin_id = :dienst_id");

        // SQL-Abfrage ausführen
        $query->execute(array(':dienst_id' => $dienst_id));

        // Erfolgsmeldung
        return "Dienstzeit erfolgreich bestätigt.";
    } catch (PDOException $e) {
        // Fehlermeldung
        return "Fehler beim Bestätigen der Dienstzeit: " . $e->getMessage();
    }
}

// Überprüfen, ob das Formular abgesendet wurde
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Überprüfen, ob alle erforderlichen Felder gesetzt sind
    if (isset($_POST['action'], $_POST['dienst_id'])) {
        if ($_POST['action'] === 'aktualisieren') {
            // Dienstzeiten aktualisieren und Status auf NACHBEARBEITET setzen
            $message = updateServiceTime($_POST['dienst_id'], $_POST['neuer_beginn'], $_POST['neues_ende']);
        } elseif ($_POST['action'] === 'bestaetigen') {
            // Dienstzeit bestätigen und Status auf NACHBEARBEITET setzen
            $message = confirmServiceTime($_POST['dienst_id']);
        }
    } else {
        // Fehlermeldung, wenn nicht alle Felder gesetzt sind
        $message = "Ungültige Anfrage.";
    }
}

try {
    // Datenbankverbindung herstellen
    $db = pdo(); // Verwende die pdo() Funktion aus der Konfigurationsdatei

    // SQL-Abfrage vorbereiten
    $query = $db->prepare("SELECT termine.termin_id, termine.datum, termine.beginn, termine.ende, mitarbeiter.Vorname, mitarbeiter.Nachname, arbeitsbereiche.Name AS Arbeitsbereich
                            FROM termine
                            LEFT JOIN dienst_mitarbeiter ON termine.termin_id = dienst_mitarbeiter.dienst_id
                            LEFT JOIN mitarbeiter ON dienst_mitarbeiter.mitarbeiter_id = mitarbeiter.MitarbeiterID
                            LEFT JOIN arbeitsbereiche ON termine.arbeitsbereich_id = arbeitsbereiche.ArbeitsbereichID");

    // SQL-Abfrage ausführen
    $query->execute();

    // Ergebnis der Abfrage abrufen
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

} catch (PDOException $e) {
    // Fehlerbehandlung für die Datenbankverbindung
    die("Verbindung fehlgeschlagen: " . $e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dienstplan</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .header, .btn-primary, th {
        
            background-color: #6B8E23;
            color: #ffffff;
            padding: 10px 0;
            text-align: center;
        }
        .profile-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            object-fit: cover;
            margin-bottom: 20px;
        }
        .card-custom {
            box-shadow: 0 0 15px rgba(0,0,0,0.2);
            border-radius: 10px;
        }
        .rating {
            unicode-bidi: bidi-override;
            direction: rtl;
            font-size: 1.5em;
        }
        .rating > span {
            cursor: pointer;
        }
        .rating > span:hover,
        .rating > span:hover ~ span {
            color: gold;
        }
        .detail-section {
            margin-bottom: 20px;
        }
        .detail-title {
            font-size: 18px;
            font-weight: bold;
            color: #6B8E23;
        }
        .main-title {
            font-size: 30px;
            font-weight: bold;
            color: #6B8E23;
        }

        /* Styling für die Tabelle */
        table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 10px; /* Abstand zwischen den Zellen */
        
        }

        th, td {
            padding: 10px; /* Zellenabstand */
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #6B8E23;
        }

        form {
            display: inline-block;
        }
    </style>
</head>
<body>

<h2 class =header>Dienstplan</h2>

<table>
    <thead>
    <tr>
        <th>DienstID</th>
        <th>Datum</th>
        <th>Beginn</th>
        <th>Ende</th>
        <th>Eingeteilter Mitarbeiter</th>
        <th>Arbeitsbereich</th>
        <th>Aktionen</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
        <tr>
            <td><?php echo $row['termin_id']; ?></td>
            <td><?php echo $row['datum']; ?></td>
            <td><?php echo $row['beginn']; ?></td>
            <td><?php echo $row['ende']; ?></td>
            <td><?php echo $row['Vorname'] . ' ' . $row['Nachname']; ?></td>
            <td><?php echo $row['Arbeitsbereich']; ?></td>
            <td>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <input type="hidden" name="dienst_id" value="<?php echo $row['termin_id']; ?>">
                    <input type="hidden" name="neuer_beginn" value="<?php echo $row['beginn']; ?>">
                    <input type="hidden" name="neues_ende" value="<?php echo $row['ende']; ?>">
                    <button type="submit" name="action" value="bestaetigen">Bestätigen</button>
                </form>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <input type="hidden" name="dienst_id" value="<?php echo $row['termin_id']; ?>">
                    <label for="neuer_beginn_<?php echo $row['termin_id']; ?>">Neuer Beginn:</label>
                    <input type="time" id="neuer_beginn_<?php echo $row['termin_id']; ?>" name="neuer_beginn" required>
                    <label for="neues_ende_<?php echo $row['termin_id']; ?>">Neues Ende:</label>
                    <input type="time" id="neues_ende_<?php echo $row['termin_id']; ?>" name="neues_ende" required>
                    <button type="submit" name="action" value="aktualisieren">Aktualisieren</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php if (isset($message)) : ?>
    <p><?php echo $message; ?></p>
<?php endif; ?>

</body>
</html>
